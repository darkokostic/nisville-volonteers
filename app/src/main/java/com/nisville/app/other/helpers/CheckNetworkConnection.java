package com.nisville.app.other.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckNetworkConnection {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String checkConnectionType(Context context) {
        String connectionType = "";
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase(Constants.INTERNET_TYPE_WIFI))
                if (ni.isConnected())
                    connectionType = Constants.INTERNET_TYPE_WIFI;
            if (ni.getTypeName().equalsIgnoreCase(Constants.INTERNET_TYPE_MOBILE))
                if (ni.isConnected())
                    connectionType = Constants.INTERNET_TYPE_MOBILE;
        }
        return connectionType;
    }
}
