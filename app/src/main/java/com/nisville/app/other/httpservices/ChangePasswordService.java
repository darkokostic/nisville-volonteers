package com.nisville.app.other.httpservices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.nisville.app.other.helpers.Constants;

public class ChangePasswordService implements Runnable {
    public ChangePasswordService.changePassword caller;

    public interface changePassword {
        public void getResponse(String data);
    }

    public ChangePasswordService(ChangePasswordService.changePassword caller) {
        this.caller = caller;
    }

    private Activity parent;
    private ProgressDialog overlay;
    private JSONObject params;
    private String token;

    public void changePassword(JSONObject jsonParams, String token, Activity parent) {
        this.parent = parent;
        this.overlay = new ProgressDialog(parent);
        this.overlay.setMessage(Constants.PLEASE_WAIT);
        this.overlay.setCancelable(false);
        this.params = jsonParams;
        this.token = token;

        Thread t = new Thread(this);
        t.start();
    }

    public void run() {
        threadMsg(sendRequest(this.params, this.token, this.parent, this.overlay));
    }

    private void threadMsg(String msg) {
        if (!msg.equals(null) && !msg.equals("")) {
            Message msgObj = handler.obtainMessage();
            Bundle b = new Bundle();
            b.putString("message", msg);
            msgObj.setData(b);
            handler.sendMessage(msgObj);
        }
    }

    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String Response = msg.getData().getString("message");
            caller.getResponse(Response);
        }
    };

    public static String sendRequest(final JSONObject params, final String token, final Activity parent, final ProgressDialog overlay) {
        URL website;
        StringBuilder response = null;
        int code = 0;
        String returnData = "";
        parent.runOnUiThread(new Runnable() {
            public void run() {
                overlay.show();
            }
        });

        try {
            website = new URL(Constants.CHANGE_PASSWORD_API + "?token=" + token);
            HttpURLConnection connection = (HttpURLConnection) website.openConnection();
            String bearerAuth = "Bearer " + token;
            connection.setRequestProperty ("Authorization", bearerAuth);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();

            OutputStream os = connection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

            osw.write(params.toString());
            osw.flush();
            osw.close();

            code = connection.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();

        } catch (final Exception  e) {
            parent.runOnUiThread(new Runnable() {
                public void run() {
                    overlay.dismiss();
                    Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                }
            });
        }

        if(code == 200) {
            try {
                final JSONObject data = new JSONObject(response.toString());
                final String outcome = data.getString(Constants.OUTCOME);

                if(outcome.equals(Constants.OUTCOME_SUCCESS)) {
                    returnData = response.toString();
                    parent.runOnUiThread(new Runnable() {
                        public void run() {
                            overlay.dismiss();
                        }
                    });
                } else if(outcome.equals(Constants.OUTCOME_FAILED)) {
                    returnData = response.toString();
                    parent.runOnUiThread(new Runnable() {
                        public void run() {
                            overlay.dismiss();
                        }
                    });
                } else {
                    parent.runOnUiThread(new Runnable() {
                        public void run() {
                            overlay.dismiss();
                            Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (final JSONException e) {
                parent.runOnUiThread(new Runnable() {
                    public void run() {
                        overlay.dismiss();
                        Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            parent.runOnUiThread(new Runnable() {
                public void run() {
                    overlay.dismiss();
                    Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                }
            });
        }
        return returnData;
    }
}
