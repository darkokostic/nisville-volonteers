package com.nisville.app.other.models;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Nastup implements Serializable {

    private ArrayList<String> listaDatuma;
    private HashMap<String, ArrayList<ArrayList<String>>> listaNastupa;

    public ArrayList<String> getListaDatuma() {
        return listaDatuma;
    }

    public void setListaDatuma(ArrayList<String> listaDatuma) {
        this.listaDatuma = listaDatuma;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getListaNastupa() {
        return listaNastupa;
    }

    public void setListaNastupa(HashMap<String, ArrayList<ArrayList<String>>> listaNastupa) {
        this.listaNastupa = listaNastupa;
    }
}
