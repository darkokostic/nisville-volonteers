package com.nisville.app.other.models;

import java.io.Serializable;

public class Stage implements Serializable {

    private String ime;
    private int id;

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
