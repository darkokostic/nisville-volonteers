package com.nisville.app.other.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalStorage {

    // sync time
    public static void setHostFullSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.HOST_FULL_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static void setHostMuzicariSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.HOST_MUZICARI_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getHostMuzicariSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.HOST_MUZICARI_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.HOST_MUZICARI_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.HOST_FULL_SYNC_TIME, "");
        }
    }

    public static void setHostPocetnaSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.HOST_POCETNA_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getHostPocetnaSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.HOST_POCETNA_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.HOST_POCETNA_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.HOST_FULL_SYNC_TIME, "");
        }
    }

    public static void setHostBrojeviTelefonaSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.HOST_BROJEVI_TELEFONA_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getHostBrojeviTelefonaSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.HOST_BROJEVI_TELEFONA_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.HOST_BROJEVI_TELEFONA_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.HOST_FULL_SYNC_TIME, "");
        }
    }

    public static void setSaradnikFullSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_FULL_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static void setSaradnikMuzicariSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_MUZICARI_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getSaradnikMuzicariSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.SARADNIK_MUZICARI_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_MUZICARI_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    public static void setSaradnikNastupiSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_NASTUPI_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getSaradnikNastupiSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.SARADNIK_NASTUPI_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_NASTUPI_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    public static void setSaradnikTonskeProbeSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_TONSKE_PROBE_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getSaradnikTonskeProbeSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.SARADNIK_TONSKE_PROBE_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_TONSKE_PROBE_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    public static void setSaradnikPressSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_PRESS_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getSaradnikPressSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.SARADNIK_PRESS_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_PRESS_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    public static void setSaradnikWorkshopSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_WORKSHOP_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getSaradnikWorkshopSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.SARADNIK_WORKSHOP_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_WORKSHOP_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    public static void setSaradnikPrevozSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_PREVOZ_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getSaradnikPrevozSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.SARADNIK_PREVOZ_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_PREVOZ_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    public static void setSaradnikNocenjaSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_NOCENJA_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getSaradnikNocenjaSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.SARADNIK_NOCENJA_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_NOCENJA_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    public static void setStagesSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.SARADNIK_STAGES_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getMuzicarSinglehHostSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.MUZICAR_SINGLE_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.MUZICAR_SINGLE_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.HOST_FULL_SYNC_TIME, "");
        }
    }

    public static void setMuzicarSingleHostSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.MUZICAR_SINGLE_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getMuzicarSinglehSaradnikSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.MUZICAR_SINGLE_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.MUZICAR_SINGLE_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    public static void setMuzicarSingleSaradnikSyncTime(Context context, String syncTime) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = syncTimeSharedPreference.edit();
        edit.putString(Constants.MUZICAR_SINGLE_SYNC_TIME, syncTime);
        edit.apply();
    }

    public static String getStegesSyncTime(Context context) {
        SharedPreferences syncTimeSharedPreference = context.getSharedPreferences(Constants.SYNC_TIME, Context.MODE_PRIVATE);
        if(syncTimeSharedPreference.contains(Constants.SARADNIK_STAGES_SYNC_TIME)) {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_STAGES_SYNC_TIME, "");
        } else {
            return syncTimeSharedPreference.getString(Constants.SARADNIK_FULL_SYNC_TIME, "");
        }
    }

    // local data
    public static void setLocalData(Context context, String key, String data) {
        SharedPreferences localDataSharedPreference = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = localDataSharedPreference.edit();
        edit.putString(key, data);
        edit.apply();
    }

    public static String getLocalData(Context context, String key) {
        SharedPreferences localDataSharedPreference = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE);
        return localDataSharedPreference.getString(key, "");
    }

    // check if muzicar exist
    public static boolean checkIfMuzicarExist(Context context, int id) {
        SharedPreferences localData = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE);

        return localData.contains(Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA + id);
    }

    // check if user exist
    public static boolean checkIfUserExist(Context context) {
        SharedPreferences localData = context.getSharedPreferences(Constants.LOCAL_DATA, Context.MODE_PRIVATE);

        return localData.contains(Constants.USER_ACCESS);
    }

    // only wifi toggle button state
    public static boolean checkIfToggleStateExist(Context context) {
        SharedPreferences toggleButtonData = context.getSharedPreferences(Constants.TOGGLE_BUTTON, Context.MODE_PRIVATE);

        return toggleButtonData.contains(Constants.TOGGLE_BUTTON_STATE);
    }

    public static void setToggleButtonState(Context context, String data) {
        SharedPreferences toggleButtonData = context.getSharedPreferences(Constants.TOGGLE_BUTTON, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = toggleButtonData.edit();
        edit.putString(Constants.TOGGLE_BUTTON_STATE, data);
        edit.apply();
    }

    public static String getToggleButtonState(Context context) {
        SharedPreferences toggleButtonData = context.getSharedPreferences(Constants.TOGGLE_BUTTON, Context.MODE_PRIVATE);
        return toggleButtonData.getString(Constants.TOGGLE_BUTTON_STATE, "");
    }

    // set token
    public static void setToken(Context context, String data) {
        SharedPreferences toggleButtonData = context.getSharedPreferences(Constants.TOKEN_LOCAL_STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = toggleButtonData.edit();
        edit.putString(Constants.TOKEN, data);
        edit.apply();
    }

    // get token
    public static String getToken(Context context) {
        SharedPreferences toggleButtonData = context.getSharedPreferences(Constants.TOKEN_LOCAL_STORAGE, Context.MODE_PRIVATE);
        return toggleButtonData.getString(Constants.TOKEN, "");
    }

    // remove token
    public static void removeToken(Context context) {
        SharedPreferences toggleButtonData = context.getSharedPreferences(Constants.TOKEN_LOCAL_STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = toggleButtonData.edit();
        edit.remove(Constants.TOKEN);
        edit.apply();
    }

    // Active Fragments
    // set fragment
    public static void setActiveFragment(Context context, String fragmentName) {
        SharedPreferences activityPref = context.getSharedPreferences(Constants.ACTIVE_FRAGMENT_STORAGE, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = activityPref.edit();
        edit.putString(Constants.ACTIVE_FRAGMENT_KEY, fragmentName);
        edit.apply();
    }

    // get fragment
    public static String getActiveFragment(Context context) {
        SharedPreferences activityPref = context.getSharedPreferences(Constants.ACTIVE_FRAGMENT_STORAGE, Context.MODE_PRIVATE);
        return activityPref.getString(Constants.ACTIVE_FRAGMENT_KEY, "");
    }

    // check if token exist
    public static boolean checkIfTokenExist(Context context) {
        SharedPreferences localToken = context.getSharedPreferences(Constants.TOKEN_LOCAL_STORAGE, Context.MODE_PRIVATE);

        return localToken.contains(Constants.TOKEN);
    }
}
