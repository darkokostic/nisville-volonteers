package com.nisville.app.other.helpers;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

public class SMSSend {
    public static void send(Context context, String number) {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("smsto:" + Uri.encode(number)));
            context.startActivity(intent);
        } catch(ActivityNotFoundException e) {
            Toast.makeText(context, Constants.NO_MESSAGE_ACTIVITY_MESSAGE, Toast.LENGTH_SHORT).show();
        }
    }
}
