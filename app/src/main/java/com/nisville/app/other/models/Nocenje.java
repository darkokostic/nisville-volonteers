package com.nisville.app.other.models;

import java.io.Serializable;

public class Nocenje implements Serializable {

    private String soba;
    private String pocetak;
    private String kraj;
    private String accomodation;
    private String musician;
    private int brojClanova;

    public String getSoba() {
        return soba;
    }

    public void setSoba(String soba) {
        this.soba = soba;
    }

    public String getPocetak() {
        return pocetak;
    }

    public void setPocetak(String pocetak) {
        this.pocetak = pocetak;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public String getAccomodation() {
        return accomodation;
    }

    public void setAccomodation(String accomodation) {
        this.accomodation = accomodation;
    }

    public String getMusician() {
        return musician;
    }

    public void setMusician(String musician) {
        this.musician = musician;
    }

    public int getBrojClanova() {
        return brojClanova;
    }

    public void setBrojClanova(int brojClanova) {
        this.brojClanova = brojClanova;
    }
}