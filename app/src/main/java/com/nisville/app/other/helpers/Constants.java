package com.nisville.app.other.helpers;

public class Constants {

    // host url
//    public static final String HOST_URL = "http://160.99.37.250/nisvilleApi/public/api";
    public static final String HOST_URL = "https://nisville.info/API";

    // host services
    public static final String HOST_FULL_SYNC_API = HOST_URL + "/mobile/get/fullsync";
    public static final String HOST_POCETNA_API = HOST_URL + "/mobile/get/announcements";
    public static final String HOST_MUZICARI_LISTA_API = HOST_URL + "/mobile/get/hostmusicians";
    public static final String HOST_BROJEVI_TELEFONA_API = HOST_URL + "/mobile/get/phonenumbers";

    // saradnik services
    public static final String SARADNIK_FULL_SYNC_API = HOST_URL + "/mobile/get/fullsync";
    public static final String SARADNIK_MUZICARI_LISTA_API = HOST_URL + "/mobile/get/musician";
    public static final String SARADNIK_NASTUPI_LISTA_API = HOST_URL + "/mobile/get/stage/performance";
    public static final String SARADNIK_TONSKE_PROBE_LISTA_API = HOST_URL + "/mobile/get/stage/soundcheck";
    public static final String SARADNIK_PRESS_LISTA_API = HOST_URL + "/mobile/get/musiciantime/press";
    public static final String SARADNIK_WORKSHOP_LISTA_API = HOST_URL + "/mobile/get/musiciantime/workshop";
    public static final String SARADNIK_PREVOZ_LISTA_API = HOST_URL + "/mobile/get/transport";
    public static final String SARADNIK_NOCENJA_LISTA_API = HOST_URL + "/mobile/get/nights";
    public static final String SARADNIK_STAGES_LISTA_API = HOST_URL + "/mobile/get/stages";

    // muzicar single service
    public static final String MUZICAR_SINGLE_API = HOST_URL + "/mobile/get/musician";

    // login service
    public static final String LOGIN_API = HOST_URL + "/mobile/login/getmbtoken";
    public static final String CHECK_TOKEN_API = HOST_URL + "/mobile/login/checktoken";
    public static final String CHANGE_PASSWORD_API = HOST_URL + "/mobile/login/changedefaultpass";

    // LocalStorage Shared Preference name
    public static final String LOCAL_DATA = "LocalData";
    public static final String SYNC_TIME = "SyncTime";
    public static final String TOGGLE_BUTTON = "ToggleButton";
    public static final String TOKEN_LOCAL_STORAGE = "tokenStorage";

    // LocalStorage Local Data keys
    public static final String HOST_FULL_SYNC_LOCAL_STORAGE_DATA = "hostFullSync";
    public static final String HOST_MUZICARI_LOCAL_STORAGE_DATA = "hostMuzicari";
    public static final String HOST_POCETNA_LOCAL_STORAGE_DATA = "hostPocetna";
    public static final String HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA = "hostBrojeviTelefona";

    public static final String SARADNIK_FULL_SYNC_LOCAL_STORAGE_DATA = "saradnikFullSync";
    public static final String SARADNIK_MUZICARI_LOCAL_STORAGE_DATA = "saradnikMuzicari";
    public static final String SARADNIK_NASTUPI_LOCAL_STORAGE_DATA = "saradnikNastupi";
    public static final String SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA = "saradnikTonskeProbe";
    public static final String SARADNIK_PRESS_LOCAL_STORAGE_DATA = "saradnikPress";
    public static final String SARADNIK_WORKSHOP_LOCAL_STORAGE_DATA = "saradnikWorkshop";
    public static final String SARADNIK_PREVOZ_LOCAL_STORAGE_DATA = "saradnikPrevoz";
    public static final String SARADNIK_NOCENJA_LOCAL_STORAGE_DATA = "saradnikNocenja";
    public static final String SARADNIK_STAGES_LOCAL_STORAGE_DATA = "stages";

    public static final String MUZICAR_SINGLE_LOCAL_STORAGE_DATA = "muzicar";

    // LocalStorage Sync Data keys
    public static final String HOST_FULL_SYNC_TIME = "hostFullsyncTime";
    public static final String HOST_MUZICARI_SYNC_TIME = "hostMuzicarisyncTime";
    public static final String HOST_POCETNA_SYNC_TIME = "hostPocetnasyncTime";
    public static final String HOST_BROJEVI_TELEFONA_SYNC_TIME = "hostBrojeviTelefonasyncTime";

    public static final String SARADNIK_FULL_SYNC_TIME = "saradnikFullsyncTime";
    public static final String SARADNIK_MUZICARI_SYNC_TIME = "saradnikMuzicarisyncTime";
    public static final String SARADNIK_NASTUPI_SYNC_TIME = "saradnikNastupisyncTime";
    public static final String SARADNIK_TONSKE_PROBE_SYNC_TIME = "saradnikTonskeProbesyncTime";
    public static final String SARADNIK_PRESS_SYNC_TIME = "saradnikPresssyncTime";
    public static final String SARADNIK_WORKSHOP_SYNC_TIME = "saradnikWorkshopsyncTime";
    public static final String SARADNIK_PREVOZ_SYNC_TIME = "saradnikPrevozsyncTime";
    public static final String SARADNIK_NOCENJA_SYNC_TIME = "saradnikNocenjasyncTime";
    public static final String SARADNIK_STAGES_SYNC_TIME = "stagesSyncTime";

    public static final String USER_NAME = "username";
    public static final String USER_TYPE = "type";
    public static final String MUZICAR_SINGLE_SYNC_TIME = "muzicarSyncTime";

    // LocalStorage Toggle Button state key
    public static final String TOGGLE_BUTTON_STATE = "toggleButtonState";

    // LocalStorage Token
    public static final String TOKEN = "token";

    // LocalStorage Active Fragment Constant
    public static final String ACTIVE_FRAGMENT_STORAGE = "ActivityInfo";
    public static final String ACTIVE_FRAGMENT_KEY = "activity";

    // Fragments Storage Keys
    public static final String HOST_FRAGMENT_MUZICARI = "hostMuzicari";
    public static final String SARADNIK_FRAGMENT_MUZICARI = "saradnikMuzicari";
    public static final String SARADNIK_FRAGMENT_TONSKE_PROBE = "tonske";
    public static final String SARADNIK_FRAGMENT_NASTUPI = "nastupi";
    public static final String SARADNIK_FRAGMENT_PRESS = "press";
    public static final String SARADNIK_FRAGMENT_PREVOZI = "prevozi";
    public static final String SARADNIK_FRAGMENT_RADIONICE = "radionice";
    public static final String SARADNIK_FRAGMENT_NOCENJA = "nocenja";

    // JSON in Response keys
    public static final String RESULT = "result";
    public static final String OUTCOME = "outcome";
    public static final String TOKEN_JSON = "token";
    public static final String NEW_TOKEN = "new_token";
    public static final String NAME_JSON = "name";
    public static final String DISPLAY_DATA = "display_data";
    public static final String ACCESS = "access";
    public static final String TYPE = "type";
    public static final String SYNC = "sync";
    public static final String OUTCOME_INVALID_CREDENTIALS = "invalid_credentials";
    public static final String OUTCOME_USER_INACTIVE = "user_inactive";
    public static final String OUTCOME_TOKEN_EXPIRED = "token_exipred";
    public static final String OUTCOME_TOKEN_INVALID = "token_invalid";
    public static final String OUTCOME_USER_NOT_FOUND = "user_not_found";
    public static final String OUTCOME_PASSWORD_CHANGED = "default_password_changed";
    public static final String OUTCOME_FIRST_LOGIN = "firstLogin";
    public static final String PASSWORD_CHANGE_TOKEN = "password_change_token";
    public static final String ERROR = "error";
    public static final String OUTCOME_SUCCESS = "success";
    public static final String OUTCOME_FAILED = "failed";
    public static final String DATA_JSON = "data";
    public static final String UP_TO_DATE_JSON = "up_to_date";
    public static final String LAST_SYNC_JSON = "lastSync";

    // host JSON in Response keys
    public static final String HOST_HOME_JSON = "home";
    public static final String HOST_MOJI_MUZICARI_JSON = "moji_muzicari";
    public static final String HOST_BROJEVI_TELEFONA_JSON = "brojevi_telefona";

    // saradnik JSON in Response keys
    public static final String SARADNIK_MUZICARI_JSON = "muzicari";
    public static final String SARADNIK_PRESS_JSON = "press";
    public static final String SARADNIK_WORKSHOP_JSON = "workshop";
    public static final String SARADNIK_PREVOZ_JSON = "prevoz";
    public static final String SARADNIK_NOCENJA_JSON = "nocenja";
    public static final String SARADNIK_STAGES_JSON = "stages";
    public static final String SARADNIK_STAGE_ID_JSON = "stageID";

    // messages
    public static final String PLEASE_WAIT = "Molimo sačekajte...";
    public static final String SUCCESSFULLY_LOGGED_IN = "Prijavljivanje uspešno";
    public static final String SUCCESSFULLY_LOGGED_OUT = "Odjavljivanje uspešno";
    public static final String NO_USERNAME_MESSAGE = "Unesi e-mail";
    public static final String NO_PASSWORD_MESSAGE = "Unesi lozinku";
    public static final String PASSWORD_CHANGED_MESSAGE = "Već je promenjena početna lozinka";
    public static final String INVALID_CREDENTIALS_MESSAGE = "Pogrešni e-mail ili lozinka";
    public static final String SERVER_NOT_RESPONSE = "Server nije dostupan, pokušaj kasnije";
    public static final String ENTER_PASSWORD = "Unesi novu lozinku";
    public static final String REPEATE_PASSWORD = "Ponovi lozinku";
    public static final String PASSWORD_MATCH = "Unete lozinke se ne poklapaju";
    public static final String LOWER_LETTER_MESSAGE = "Lozinka mora sadržati najmanje jedno malo slovo";
    public static final String UPPER_LETTER_MESSAGE = "Lozinka mora sadržati najmanje jedno veliko slovo";
    public static final String NUMBER_LETTER_MESSAGE = "Lozinka mora sadržati najmanje jedan broj";
    public static final String LENGTH_PASSWORD_MESSAGE = "Lozinka mora imati najmanje 8 karaktera";
    public static final String CHANGE_PASSWORD_TOKEN_EXPIRED = "Isteklo predvidjeno vreme za promenu lozinke, pokušaj ponovo";
    public static final String PASSWORD_SUCCESSFULLY_CHANGED = "Upsešna promena lozinke";
    public static final String USER_INACTIVE_MESSAGE = "Tvoj nalog nije aktivan";
    public static final String TOKEN_EXPIRED_MESSAGE = "Moraš ponovo da se prijaviš";
    public static final String USER_NOT_FOUND_MESSAGE = "Tvoj nalog je izbrisan";
    public static final String NO_PHONE_NUMBER_MESSAGE = "Broj telefona nije unet";
    public static final String NO_MESSAGE_ACTIVITY_MESSAGE = "Nije moguče slanje poruke sa tvog uređaja";
    public static final String NO_CALL_ACTIVITY_MESSAGE = "Nije moguče slanje pozivanje sa tvog uređaja";

    public static final String NIJE_UNETO_LABEL = "nije uneto";
    public static final String NULL_RESPONSE = "null";

    // alert
    public static final String NETWORK_CONNECTION_ALERT_MESSAGE = "Nema internet konekcije";

    // internet connection type
    public static final String INTERNET_TYPE_WIFI = "WIFI";
    public static final String INTERNET_TYPE_MOBILE = "MOBILE";

    // user access
    public static final String USER_ACCESS = "access";
    public static final String USER_TYPE_HOST = "host";

    public static final String NO_RESPONSE = "no response";

}
