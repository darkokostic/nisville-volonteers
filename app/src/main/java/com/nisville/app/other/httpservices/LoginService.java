package com.nisville.app.other.httpservices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.nisville.app.other.helpers.Constants;

public class LoginService implements Runnable {

    public login caller;

    public interface login {
        public void getResponse(String data);
    }

    public LoginService(login caller) {
        this.caller = caller;
    }

    private Activity parent;
    private ProgressDialog overlay;
    private JSONObject params;

    public void login(JSONObject jsonParams, Activity parent) {
        this.parent = parent;
        this.overlay = new ProgressDialog(parent);
        this.overlay.setMessage(Constants.PLEASE_WAIT);
        this.overlay.setCancelable(false);
        this.params = jsonParams;

        Thread t = new Thread(this);
        t.start();
    }

    public void run() {
        threadMsg(sendRequest(this.params, this.parent, this.overlay));
    }

    private void threadMsg(String msg) {
        if (!msg.equals(null) && !msg.equals("")) {
            Message msgObj = handler.obtainMessage();
            Bundle b = new Bundle();
            b.putString("message", msg);
            msgObj.setData(b);
            handler.sendMessage(msgObj);
        }
    }

    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String Response = msg.getData().getString("message");
            caller.getResponse(Response);
        }
    };

    public static String sendRequest(final JSONObject params, final Activity parent, final ProgressDialog overlay) {
        URL website;
        StringBuilder response = null;
        int code = 0;
        String returnData = "";

        parent.runOnUiThread(new Runnable() {
            public void run() {
                overlay.show();
            }
        });

        try {

            website = new URL(Constants.LOGIN_API);
            HttpURLConnection connection = (HttpURLConnection) website.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("User-Agent","Mozilla/5.0 ( compatible ) ");
            connection.setRequestProperty("Accept","*/*");
            connection.connect();

            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            dStream.writeBytes(params.toString());
            dStream.flush();
            dStream.close();

            code = connection.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();

        } catch (final Exception  e) {
            e.printStackTrace();
            parent.runOnUiThread(new Runnable() {
                public void run() {
                    overlay.dismiss();
                    Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                }
            });
        }

        if(code == 200) {
            try {
                final JSONObject data = new JSONObject(response.toString());
                final String outcome = data.getString(Constants.OUTCOME);

                if(outcome.equals(Constants.OUTCOME_SUCCESS)) {
                    returnData = response.toString();
                    parent.runOnUiThread(new Runnable() {
                        public void run() {
                            overlay.dismiss();
                        }
                    });
                } else {
                    String error = "";
                    if(data.has(Constants.ERROR)) {
                        error = data.getString(Constants.ERROR);
                    }
                    if(outcome.equals(Constants.OUTCOME_FIRST_LOGIN)) {
                        returnData = response.toString();
                        parent.runOnUiThread(new Runnable() {
                            public void run() {
                                overlay.dismiss();
                            }
                        });
                    } else if(error.equals(Constants.OUTCOME_INVALID_CREDENTIALS)) {
                        parent.runOnUiThread(new Runnable() {
                            public void run() {
                                overlay.dismiss();
                                Toast.makeText(parent, Constants.INVALID_CREDENTIALS_MESSAGE, Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if(error.equals(Constants.OUTCOME_USER_INACTIVE)) {
                        parent.runOnUiThread(new Runnable() {
                            public void run() {
                                overlay.dismiss();
                                Toast.makeText(parent, Constants.USER_INACTIVE_MESSAGE, Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if(error.equals(Constants.OUTCOME_PASSWORD_CHANGED)) {
                        parent.runOnUiThread(new Runnable() {
                            public void run() {
                                overlay.dismiss();
                                Toast.makeText(parent, Constants.PASSWORD_CHANGED_MESSAGE, Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        parent.runOnUiThread(new Runnable() {
                            public void run() {
                                overlay.dismiss();
                                Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            } catch (final JSONException e) {
                e.printStackTrace();
                parent.runOnUiThread(new Runnable() {
                    public void run() {
                        overlay.dismiss();
                        Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        return returnData;
    }
}
