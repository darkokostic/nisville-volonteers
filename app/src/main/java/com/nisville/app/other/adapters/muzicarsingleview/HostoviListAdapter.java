package com.nisville.app.other.adapters.muzicarsingleview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.other.helpers.CallNumber;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.SMSSend;
import com.nisville.app.other.models.Volonter;

public class HostoviListAdapter extends RecyclerView.Adapter<HostoviListAdapter.HostoviListHolder> {

    public interface OnItemCheckListener {
        void onItemCheck(Volonter item);
        void onItemUncheck(Volonter item);
    }

    private Context context;
    private ArrayList<Volonter> data;
    private LayoutInflater inflater;
    private boolean visibility;
    @NonNull
    public OnItemCheckListener onItemClick;

    public HostoviListAdapter(Context context, ArrayList<Volonter> data, @NonNull OnItemCheckListener onItemCheckListener) {
        this.context = context;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.onItemClick = onItemCheckListener;
    }

    public void setCheckBoxVisibility(boolean visibility) {
        this.visibility = visibility;
        notifyDataSetChanged();
    }

    @Override
    public HostoviListAdapter.HostoviListHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.card_muzicar_single_host_item, parent, false);
        final HostoviListAdapter.HostoviListHolder holder = new HostoviListAdapter.HostoviListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(HostoviListAdapter.HostoviListHolder holder, final int position) {
        final Volonter infoData = data.get(position);

        holder.txtLabel.setText("Host " + (position + 1));
        holder.txtIme.setText(data.get(position).getIme());
        holder.txtBrojTelefona.setText(data.get(position).getBrojTelefona());

        if(holder.txtBrojTelefona.getText().equals(Constants.NULL_RESPONSE)) {
            holder.txtBrojTelefona.setVisibility(View.GONE);
            holder.btnCall.setVisibility(View.GONE);
            holder.btnMessage.setVisibility(View.GONE);
        }

        if(this.visibility) {
            holder.cardViewHost.setClickable(true);
            holder.cbShare.setChecked(false);
            holder.cbShare.setVisibility(View.VISIBLE);
        } else {
            holder.cardViewHost.setClickable(false);
            holder.cbShare.setChecked(false);
            holder.cbShare.setVisibility(View.GONE);
        }

        final CardView cardView = holder.cardViewHost;
        final HostoviListAdapter.HostoviListHolder newHolder = holder;

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!newHolder.cbShare.isChecked()) {
                    newHolder.cbShare.setChecked(true);
                    onItemClick.onItemCheck(infoData);
                } else {
                    newHolder.cbShare.setChecked(false);
                    onItemClick.onItemUncheck(infoData);
                }
            }
        });

        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CallNumber.call(context, infoData.getBrojTelefona());

            }
        });

        holder.btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SMSSend.send(context, infoData.getBrojTelefona());

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class HostoviListHolder extends RecyclerView.ViewHolder {

        public TextView txtLabel;
        public TextView txtBrojTelefona;
        public TextView txtIme;
        public ImageView btnCall;
        public ImageView btnMessage;
        public CheckBox cbShare;
        public CardView cardViewHost;

        public HostoviListHolder(View itemView) {
            super(itemView);
            txtLabel = (TextView) itemView.findViewById(R.id.txtHostMuzicarSingleHostLabel);
            txtIme = (TextView) itemView.findViewById(R.id.txtHostMuzicarSingleHostName);
            txtBrojTelefona = (TextView) itemView.findViewById(R.id.txtHostMuzicarSingleHostPhoneNumber);
            btnCall = (ImageView) itemView.findViewById(R.id.btnHostMuzicarSingleCall);
            btnMessage = (ImageView) itemView.findViewById(R.id.btnHostMuzicarSingleMessage);
            cbShare = (CheckBox) itemView.findViewById(R.id.cbMuzicarSingleHost);
            cardViewHost = (CardView) itemView.findViewById(R.id.card_view_host);
        }
    }
}
