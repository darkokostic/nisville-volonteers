package com.nisville.app.other.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.nisville.app.R;
import com.nisville.app.other.models.Prevoz;

public class ExpListPrevoziAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> itemsData;
    private HashMap<String, ArrayList<ArrayList<Prevoz>>> subitemsData;

    public ExpListPrevoziAdapter(Context context, List<String> itemsData, HashMap<String, ArrayList<ArrayList<Prevoz>>> subitemsData) {
        this.context = context;
        this.itemsData = itemsData;
        this.subitemsData = subitemsData;
    }

    @Override
    public int getGroupCount() {
        return this.itemsData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.subitemsData.get(this.itemsData.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.itemsData.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.subitemsData.get(this.itemsData.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        final String item = (String) getGroup(groupPosition);

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exp_list_with_swipe_item, null);
        }

        TextView txtItem = (TextView) convertView.findViewById(R.id.txt_exp_list_prevozi_item);
        ImageView arrow = (ImageView) convertView.findViewById(R.id.imgArrow);
        TextView txtVidiVise = (TextView) convertView.findViewById(R.id.txtExpPrevoziVidiVise);

        if(isExpanded) {
            arrow.setImageResource(R.drawable.arrow_up);
            txtVidiVise.setTextColor(this.context.getResources().getColor(R.color.colorIndicatorExpanded));
        } else {
            arrow.setImageResource(R.drawable.arrow_down);
            txtVidiVise.setTextColor(this.context.getResources().getColor(R.color.colorIndicator));
        }

        txtItem.setText(item);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final ArrayList<Prevoz> subItem = (ArrayList<Prevoz>) getChild(groupPosition, childPosition);

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.exp_list_with_swipe_subitem, null);

        RecyclerView rv = (RecyclerView) convertView.findViewById(R.id.saradnik_with_swipe_subitem_recyclerview);
        rv.setHasFixedSize(true);
        PrevoziSubItemAdapter adapter = new PrevoziSubItemAdapter(this.context, subItem);
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(llm);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
