package com.nisville.app.other.adapters.muzicarsingleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nisville.app.other.helpers.ShareContent;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import com.nisville.app.R;
import com.nisville.app.other.helpers.Constants;

public class SmestajSliderAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listItem;
    private HashMap<String, List<String>> listSubitem;
    private boolean visibility;
    private CheckBox cbShare;

    public SmestajSliderAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
        this.context = context;
        this.listItem = listDataHeader;
        this.listSubitem = listChildData;
    }

    public void setCheckBoxVisibility(boolean visibility) {
        this.visibility = visibility;
        notifyDataSetChanged();
    }

    public boolean getIsShareable() {
        return cbShare.isChecked();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listSubitem.get(this.listItem.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String smestajChild = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_list_muzicar_single_smestaj_subitem, null);
        }

        try {
            CarouselView smestajCarousel = (CarouselView) convertView.findViewById(R.id.smestajCarouselView);
            final JSONArray sliderData = new JSONArray(smestajChild);
            if(sliderData.length() > 0) {
                smestajCarousel.setPageCount(sliderData.length());
                smestajCarousel.setViewListener(new ViewListener() {
                    @Override
                    public View setViewForPosition(int position) {
                        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View customView = infalInflater.inflate(R.layout.smestaj_muzicar_single_swipe_slider, null);
                        try {
                            JSONObject smestajItem = new JSONObject(sliderData.get(position).toString());
                            final TextView txtHotel = (TextView) customView.findViewById(R.id.txt_hotel);
                            final TextView txtBrojLjudi = (TextView) customView.findViewById(R.id.txt_broj_ljudi);
                            final TextView txtSoba = (TextView) customView.findViewById(R.id.txt_soba);
                            final TextView txtPocetak = (TextView) customView.findViewById(R.id.txt_pocetak_smestaj);
                            final TextView txtKraj = (TextView) customView.findViewById(R.id.txt_kraj_smestaj);

                            if(!smestajItem.getString("accomodation").equals(Constants.NULL_RESPONSE)) {
                                JSONObject hotel = new JSONObject(smestajItem.getString("accomodation"));
                                txtHotel.setText(hotel.getString("ime"));
                            } else {
                                txtHotel.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!smestajItem.getString("brojljudi").equals(Constants.NULL_RESPONSE)) {
                                txtBrojLjudi.setText(smestajItem.getInt("brojljudi") + "");
                            } else {
                                txtBrojLjudi.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!smestajItem.getString("soba").equals(Constants.NULL_RESPONSE)) {
                                txtSoba.setText(smestajItem.getString("soba"));
                            } else {
                                txtSoba.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!smestajItem.getString("pocetak").equals(Constants.NULL_RESPONSE)) {
                                txtPocetak.setText(smestajItem.getString("pocetak"));
                            } else {
                                txtPocetak.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!smestajItem.getString("kraj").equals(Constants.NULL_RESPONSE)) {
                                txtKraj.setText(smestajItem.getString("kraj"));
                            } else {
                                txtKraj.setText(Constants.NIJE_UNETO_LABEL);
                            }

                            ImageView btnShare = (ImageView) customView.findViewById(R.id.btn_share_muzicar_smestaj);

                            btnShare.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ShareContent.share(context, "Hotel: " + txtHotel.getText() + "\n" + "Broj ljudi: " + txtBrojLjudi.getText() + "\n" + "Soba: " + txtSoba.getText() + "\n" + "Od: " + txtPocetak.getText() + "\n" + "Do: " + txtKraj.getText());
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return customView;
                    }
                });
            } else {
                smestajCarousel.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listSubitem.get(this.listItem.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listItem.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listItem.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {
        final String item = (String) getGroup(groupPosition);

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exp_list_muzicar_single_smestaj_item, null);
        }

        TextView txtItem = (TextView) convertView.findViewById(R.id.txt_exp_list_single_muzicar_smestaj_name);
        ImageView arrow = (ImageView) convertView.findViewById(R.id.imgArrow);
        CheckBox cbShareSmestaj = (CheckBox) convertView.findViewById(R.id.cbMuzicarSingleSmestaj);
        cbShare = cbShareSmestaj;

        if(this.visibility) {
            cbShareSmestaj.setClickable(true);
            cbShareSmestaj.setChecked(false);
            cbShareSmestaj.setVisibility(View.VISIBLE);
        } else {
            cbShareSmestaj.setClickable(false);
            cbShareSmestaj.setChecked(false);
            cbShareSmestaj.setVisibility(View.GONE);
        }

        if(isExpanded) {
            arrow.setImageResource(R.drawable.arrow_up);
            txtItem.setTextColor(this.context.getResources().getColor(R.color.colorIndicatorExpanded));
        } else {
            arrow.setImageResource(R.drawable.arrow_down);
            txtItem.setTextColor(this.context.getResources().getColor(R.color.colorIndicator));
        }

        txtItem.setText(item);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}