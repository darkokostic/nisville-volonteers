package com.nisville.app.other.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.other.helpers.ShareContent;
import com.nisville.app.other.models.Prevoz;

public class PrevoziSubItemAdapter extends RecyclerView.Adapter<PrevoziSubItemAdapter.PrevoziSubItemViewHolder> {

    private Context context;
    private ArrayList<Prevoz> data;
    private LayoutInflater inflater;

    public PrevoziSubItemAdapter(Context context, ArrayList<Prevoz> data) {
        this.context = context;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public PrevoziSubItemAdapter.PrevoziSubItemViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.exp_list_prevozi_subitem_card, parent, false);
        PrevoziSubItemAdapter.PrevoziSubItemViewHolder holder = new PrevoziSubItemAdapter.PrevoziSubItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PrevoziSubItemAdapter.PrevoziSubItemViewHolder holder, final int position) {

        holder.txtImeMuzicara.setText(data.get(position).getImeMuzicara());
        holder.txtUcesnik.setText(data.get(position).getUcesnik());
        holder.txtBrojClanova.setText(data.get(position).getBrojClanova() + "");
        holder.txtPolazakMesto.setText(data.get(position).getPolazakMesto());
        holder.txtPolazakDatum.setText(data.get(position).getPolazakDatum());
        holder.txtPolazakVreme.setText(data.get(position).getPolazakVreme());
        holder.txtDolazakMesto.setText(data.get(position).getDolazakMesto());
        holder.txtDolazakDatum.setText(data.get(position).getDolazakDatum());
        holder.txtDolazakVreme.setText(data.get(position).getDolazakVreme());
        holder.txtVozac.setText(data.get(position).getVozac());
        holder.txtVozilo.setText(data.get(position).getVozilo());
        holder.txtHost.setText(data.get(position).getHost());

        final Prevoz infoData = data.get(position);

        holder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareContent.share(context, "Prevoz: " + "\nMuzičar: " + infoData.getImeMuzicara() + "," + "\nUčesnik: " + infoData.getUcesnik() + "," + "\nBroj članova: " + infoData.getBrojClanova() + ", " + "\nPolazak: " + infoData.getPolazakMesto() + ", " + infoData.getPolazakDatum() + infoData.getPolazakVreme() + ", " + "\nDolazak: " + infoData.getDolazakMesto() + ", " + infoData.getDolazakDatum() + infoData.getDolazakVreme() + ", " + "\nVozač: " + infoData.getVozac() + ", " + "\nVozilo: " + infoData.getVozilo() + ", " + "\nHost: " + infoData.getHost());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class PrevoziSubItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtImeMuzicara;
        public TextView txtBrojClanova;
        public TextView txtPolazakMesto;
        public TextView txtPolazakDatum;
        public TextView txtPolazakVreme;
        public TextView txtDolazakMesto;
        public TextView txtDolazakDatum;
        public TextView txtDolazakVreme;
        public TextView txtVozac;
        public TextView txtVozilo;
        public TextView txtHost;
        public TextView txtUcesnik;
        public ImageView btnShare;

        public PrevoziSubItemViewHolder(View itemView) {
            super(itemView);

            txtImeMuzicara = (TextView) itemView.findViewById(R.id.txtPrevoziSubitemIme);
            txtUcesnik = (TextView) itemView.findViewById(R.id.txtPrevoziSubitemUcesnik);
            txtBrojClanova = (TextView) itemView.findViewById(R.id.txtPrevoziSubitemBrojClanova);
            txtPolazakMesto = (TextView) itemView.findViewById(R.id.txtPrevoziSubitemPolazakMesto);
            txtPolazakDatum = (TextView) itemView.findViewById(R.id.txtPrevoziSubItemPolazakDatum);
            txtPolazakVreme = (TextView) itemView.findViewById(R.id.txtPrevoziSubItemPolazakVreme);
            txtDolazakMesto = (TextView) itemView.findViewById(R.id.txtPrevoziSubitemDolazakMesto);
            txtDolazakDatum = (TextView) itemView.findViewById(R.id.txtPrevoziSubItemDolazakDatum);
            txtDolazakVreme = (TextView) itemView.findViewById(R.id.txtPrevoziSubItemDolazakVreme);
            txtVozac = (TextView) itemView.findViewById(R.id.txtPrevoziSubitemVozac);
            txtVozilo = (TextView) itemView.findViewById(R.id.txtPrevoziSubitemVozilo);
            txtHost = (TextView) itemView.findViewById(R.id.txtPrevoziSubitemHost);
            btnShare = (ImageView) itemView.findViewById(R.id.btn_share_prevoz);
        }
    }
}
