package com.nisville.app.other.adapters.muzicarsingleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nisville.app.other.helpers.ShareContent;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import com.nisville.app.R;
import com.nisville.app.other.helpers.Constants;

public class PrevoziSliderAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listItem;
    private HashMap<String, List<String>> listSubitem;
    private boolean visibility;
    private CheckBox cbShare;

    public PrevoziSliderAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
        this.context = context;
        this.listItem = listDataHeader;
        this.listSubitem = listChildData;
    }

    public void setCheckBoxVisibility(boolean visibility) {
        this.visibility = visibility;
        notifyDataSetChanged();
    }

    public boolean getIsShareable() {
        return cbShare.isChecked();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listSubitem.get(this.listItem.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String prevoziChild = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_list_muzicar_single_prevozi_subitem, null);
        }

        try {
            CarouselView prevoziCarousel = (CarouselView) convertView.findViewById(R.id.prevoziCarouselView);
            final JSONArray sliderData = new JSONArray(prevoziChild);
            if(sliderData.length() > 0) {
                prevoziCarousel.setPageCount(sliderData.length());
                prevoziCarousel.setViewListener(new ViewListener() {
                    @Override
                    public View setViewForPosition(int position) {
                        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View customView = infalInflater.inflate(R.layout.prevozi_muzicar_single_swipe_slider, null);
                        try {
                            JSONObject prevozItem = new JSONObject(sliderData.get(position).toString());
                            final TextView txtMuzicar = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubitemIme);
                            final TextView txtUcesnik = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubitemUcesnik);
                            final TextView txtBrojLjudi = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubitemBrojClanova);
                            final TextView txtPolazakMesto = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubitemPolazakMesto);
                            final TextView txtPolazakDatum = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubItemPolazakDatum);
                            final TextView txtPolazakVreme = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubItemPolazakVreme);
                            final TextView txtDolazakMesto = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubitemDolazakMesto);
                            final TextView txtDolazakDatum = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubItemDolazakDatum);
                            final TextView txtDolazakVreme = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubItemDolazakVreme);
                            final TextView txtVozac = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubitemVozac);
                            final TextView txtVozilo = (TextView) customView.findViewById(R.id.txtMuzicarPrevoziSubitemVozilo);

                            if(!prevozItem.getString("muzicar").equals(Constants.NULL_RESPONSE)) {
                                JSONObject prevozMuzicar = new JSONObject(prevozItem.getString("muzicar"));
                                if(!prevozMuzicar.getString("ime").equals(Constants.NULL_RESPONSE)) {
                                    txtMuzicar.setText(prevozMuzicar.getString("ime"));
                                } else {
                                    txtMuzicar.setText(Constants.NIJE_UNETO_LABEL);
                                }
                            }
                            if(!prevozItem.getString("ucesnik").equals(Constants.NULL_RESPONSE)) {
                                JSONObject ucesnik = new JSONObject(prevozItem.getString("ucesnik"));
                                if(!ucesnik.getString("name").equals(Constants.NULL_RESPONSE)) {
                                    txtUcesnik.setText(ucesnik.getString("name"));
                                } else {
                                    txtUcesnik.setText(Constants.NIJE_UNETO_LABEL);
                                }
                            }
                            if(!prevozItem.getString("polazak_mesto").equals(Constants.NULL_RESPONSE)) {
                                txtPolazakMesto.setText(prevozItem.getString("polazak_mesto"));
                            } else {
                                txtPolazakMesto.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if (!prevozItem.getString("polazak").equals(Constants.NULL_RESPONSE)) {
                                JSONObject polazakVreme = new JSONObject(prevozItem.getString("polazak"));
                                txtPolazakDatum.setText(polazakVreme.getString("date") + ", ");
                                txtPolazakVreme.setText(polazakVreme.getString("time"));
                            } else {
                                txtPolazakDatum.setText(Constants.NIJE_UNETO_LABEL + ", ");
                                txtPolazakVreme.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!prevozItem.getString("dolazak_mesto").equals(Constants.NULL_RESPONSE)) {
                                txtDolazakMesto.setText(prevozItem.getString("dolazak_mesto"));
                            } else {
                                txtDolazakMesto.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if (!prevozItem.getString("dolazak").equals(Constants.NULL_RESPONSE)) {
                                JSONObject dolazakVreme = new JSONObject(prevozItem.getString("dolazak"));
                                txtDolazakDatum.setText(dolazakVreme.getString("date") + ", ");
                                txtDolazakVreme.setText(dolazakVreme.getString("time"));
                            } else {
                                txtDolazakDatum.setText(Constants.NIJE_UNETO_LABEL + ", ");
                                txtDolazakVreme.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if (!prevozItem.getString("br_ljudi").equals(Constants.NULL_RESPONSE)) {
                                txtBrojLjudi.setText(prevozItem.getString("br_ljudi"));
                            } else {
                                txtBrojLjudi.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!prevozItem.getString("vozac").equals(Constants.NULL_RESPONSE) && !prevozItem.getString("vozac_br").equals(Constants.NULL_RESPONSE)) {
                                txtVozac.setText(prevozItem.getString("vozac") + ", " + prevozItem.getString("vozac_br"));
                            } else {
                                txtVozac.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!prevozItem.getString("vehicle").equals(Constants.NULL_RESPONSE)) {
                                JSONObject vozilo = new JSONObject(prevozItem.getString("vehicle"));
                                txtVozilo.setText(vozilo.getString("tip"));
                            } else {
                                txtVozilo.setText(Constants.NIJE_UNETO_LABEL);
                            }

                            ImageView btnShare = (ImageView) customView.findViewById(R.id.btn_share_muzicar_prevoz);

                            btnShare.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ShareContent.share(context, "Muzičar: " + txtMuzicar.getText() + "\n" + "Učesnik: " + txtUcesnik.getText() + "\n" + "Broj ljudi: " + txtBrojLjudi.getText() + "\n" + "Polazak: " + txtPolazakMesto.getText() + ", " + txtPolazakDatum.getText() + txtPolazakVreme.getText() + "\n" + "Dolazak: " + txtDolazakMesto.getText() + ", " + txtDolazakDatum.getText() + txtDolazakVreme.getText() + "\n" + "Vozač: " + txtVozac.getText() + "\n" + "Vozilo: " + txtVozilo.getText());
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return customView;
                    }
                });
            } else {
                prevoziCarousel.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listSubitem.get(this.listItem.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listItem.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listItem.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {
        final String item = (String) getGroup(groupPosition);

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exp_list_muzicar_single_prevozi_item, null);
        }

        TextView txtItem = (TextView) convertView.findViewById(R.id.txt_exp_list_single_muzicar_prevozi_name);
        ImageView arrow = (ImageView) convertView.findViewById(R.id.imgArrow);
        CheckBox cbSharePrevozi = (CheckBox) convertView.findViewById(R.id.cbMuzicarSinglePrevozi);
        cbShare = cbSharePrevozi;

        if(this.visibility) {
            cbSharePrevozi.setClickable(true);
            cbSharePrevozi.setChecked(false);
            cbSharePrevozi.setVisibility(View.VISIBLE);
        } else {
            cbSharePrevozi.setClickable(false);
            cbSharePrevozi.setChecked(false);
            cbSharePrevozi.setVisibility(View.GONE);
        }

        if(isExpanded) {
            arrow.setImageResource(R.drawable.arrow_up);
            txtItem.setTextColor(this.context.getResources().getColor(R.color.colorIndicatorExpanded));
        } else {
            arrow.setImageResource(R.drawable.arrow_down);
            txtItem.setTextColor(this.context.getResources().getColor(R.color.colorIndicator));
        }

        txtItem.setText(item);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}