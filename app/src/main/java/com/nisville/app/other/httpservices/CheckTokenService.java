package com.nisville.app.other.httpservices;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.nisville.app.other.helpers.Constants;

public class CheckTokenService implements Runnable {
    public requestComplete caller;

    public interface requestComplete {
        public void getResponse(String data);
    }

    public CheckTokenService(requestComplete caller) {
        this.caller = caller;
    }

    private Activity parent;
    private String token;

    public void checkIsTokenValid(Activity parent, String token) {
        this.parent = parent;
        this.token = token;

        Thread t = new Thread(this);
        t.start();
    }

    public void run() {
        threadMsg(sendRequest(this.parent, this.token));
    }

    private void threadMsg(String msg) {

        if (!msg.equals(null) && !msg.equals("")) {
            Message msgObj = handler.obtainMessage();
            Bundle b = new Bundle();
            b.putString("message", msg);
            msgObj.setData(b);
            handler.sendMessage(msgObj);
        }
    }

    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String Response = msg.getData().getString("message");
            caller.getResponse(Response);
        }
    };

    public static String sendRequest(final Activity parent, final String token) {
        URL website;
        StringBuilder response = null;
        int code = 0;
        String returnData = "";

        try {
            website = new URL(Constants.CHECK_TOKEN_API);

            HttpURLConnection connection = (HttpURLConnection) website.openConnection();
            String bearerAuth = "Bearer " + token;
            connection.setRequestProperty ("Authorization", bearerAuth);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();

            code = connection.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();

        } catch (final Exception  e) {
            Log.i("EXCEPTION", e.toString());
            parent.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                }
            });
        }

        if(code == 200) {
            returnData = response.toString();
        } else {
            returnData = Constants.NO_RESPONSE;
        }
        return returnData;
    }
}
