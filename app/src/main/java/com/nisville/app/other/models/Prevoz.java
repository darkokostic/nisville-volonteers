package com.nisville.app.other.models;

import java.io.Serializable;

public class Prevoz implements Serializable {

    private String imeMuzicara;
    private int brojClanova;
    private String polazakMesto;
    private String polazakDatum;
    private String polazakVreme;
    private String dolazakMesto;
    private String dolazakDatum;
    private String dolazakVreme;
    private String vozac;
    private String vozilo;
    private String host;
    private String ucesnik;

    public String getImeMuzicara() {
        return imeMuzicara;
    }

    public void setImeMuzicara(String imeMuzicara) {
        this.imeMuzicara = imeMuzicara;
    }

    public String getPolazakMesto() {
        return polazakMesto;
    }

    public void setPolazakMesto(String polazakMesto) {
        this.polazakMesto = polazakMesto;
    }

    public String getVozac() {
        return vozac;
    }

    public void setVozac(String vozac) {
        this.vozac = vozac;
    }

    public int getBrojClanova() {
        return brojClanova;
    }

    public void setBrojClanova(int brojClanova) {
        this.brojClanova = brojClanova;
    }

    public String getDolazakMesto() {
        return dolazakMesto;
    }

    public void setDolazakMesto(String dolazakMesto) {
        this.dolazakMesto = dolazakMesto;
    }

    public String getVozilo() {
        return vozilo;
    }

    public void setVozilo(String vozilo) {
        this.vozilo = vozilo;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPolazakDatum() {
        return polazakDatum;
    }

    public void setPolazakDatum(String polazakDatum) {
        this.polazakDatum = polazakDatum;
    }

    public String getPolazakVreme() {
        return polazakVreme;
    }

    public void setPolazakVreme(String polazakVreme) {
        this.polazakVreme = polazakVreme;
    }

    public String getDolazakDatum() {
        return dolazakDatum;
    }

    public void setDolazakDatum(String dolazakDatum) {
        this.dolazakDatum = dolazakDatum;
    }

    public String getDolazakVreme() {
        return dolazakVreme;
    }

    public void setDolazakVreme(String dolazakVreme) {
        this.dolazakVreme = dolazakVreme;
    }

    public String getUcesnik() {
        return ucesnik;
    }

    public void setUcesnik(String ucesnik) {
        this.ucesnik = ucesnik;
    }
}
