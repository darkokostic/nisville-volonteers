package com.nisville.app.other.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.fragments.saradnik.MuzicarSaradnikActivity;
import com.nisville.app.other.models.Muzicar;


public class SaradnikMuzicariAdapter extends RecyclerView.Adapter<SaradnikMuzicariAdapter.SaradnikMuzicariViewHolder> {

    private Context context;
    private ArrayList<Muzicar> data;
    private LayoutInflater inflater;

    public SaradnikMuzicariAdapter(Context context, ArrayList<Muzicar> data) {
        this.context = context;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public SaradnikMuzicariAdapter.SaradnikMuzicariViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.card_item_muzicari, parent, false);
        SaradnikMuzicariAdapter.SaradnikMuzicariViewHolder holder = new SaradnikMuzicariAdapter.SaradnikMuzicariViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SaradnikMuzicariAdapter.SaradnikMuzicariViewHolder holder, final int position) {

        holder.txtIme.setText(data.get(position).getIme());
        holder.txtDrzava.setText(data.get(position).getDrzava());
        holder.txtDatum.setText(data.get(position).getDatum());

        holder.btnVidiVise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context.getApplicationContext(), MuzicarSaradnikActivity.class);
                i.putExtra("listaMuzicara", data);
                i.putExtra("idMuzicara", position);
                context.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class SaradnikMuzicariViewHolder extends RecyclerView.ViewHolder {

        public TextView txtIme;
        public TextView txtDrzava;
        public TextView txtDatum;
        public Button btnVidiVise;

        public SaradnikMuzicariViewHolder(View itemView) {
            super(itemView);

            txtIme = (TextView) itemView.findViewById(R.id.txtMuzicariIme);
            txtDrzava = (TextView) itemView.findViewById(R.id.txtMuzicariDrzava);
            txtDatum = (TextView) itemView.findViewById(R.id.txtMuzicariDatum);
            btnVidiVise = (Button) itemView.findViewById(R.id.btnMuzicariVidiVise);
        }
    }
}
