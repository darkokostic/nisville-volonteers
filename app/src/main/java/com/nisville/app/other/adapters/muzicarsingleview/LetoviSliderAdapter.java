package com.nisville.app.other.adapters.muzicarsingleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nisville.app.other.helpers.ShareContent;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import com.nisville.app.R;
import com.nisville.app.other.helpers.Constants;

public class LetoviSliderAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listItem;
    private HashMap<String, List<String>> listSubitem;
    private boolean visibility;
    private CheckBox cbShare;

    public LetoviSliderAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
        this.context = context;
        this.listItem = listDataHeader;
        this.listSubitem = listChildData;
    }

    public void setCheckBoxVisibility(boolean visibility) {
        this.visibility = visibility;
        notifyDataSetChanged();
    }

    public boolean getIsShareable() {
        return cbShare.isChecked();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listSubitem.get(this.listItem.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String letoviChild = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_list_muzicar_single_letovi_subitem, null);
        }

        try {
            CarouselView letoviCarousel = (CarouselView) convertView.findViewById(R.id.letoviCarouselView);
            final JSONArray sliderData = new JSONArray(letoviChild);
            if(sliderData.length() > 0) {
                letoviCarousel.setPageCount(sliderData.length());
                letoviCarousel.setViewListener(new ViewListener() {
                    @Override
                    public View setViewForPosition(int position) {
                        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View customView = infalInflater.inflate(R.layout.letovi_muzicar_single_swipe_slider, null);
                        try {
                            JSONObject letoviItem = new JSONObject(sliderData.get(position).toString());
                            final TextView txtBrojLeta = (TextView) customView.findViewById(R.id.txt_broj_leta);
                            final TextView txtPoletanje = (TextView) customView.findViewById(R.id.txt_poletanje);
                            final TextView txtPoletanjeIz = (TextView) customView.findViewById(R.id.txt_poletanje_iz);
                            final TextView txtSletanje = (TextView) customView.findViewById(R.id.txt_sletanje);
                            final TextView txtSletanjeU = (TextView) customView.findViewById(R.id.txt_sletanje_u);
                            final TextView txtBrojLjudi = (TextView) customView.findViewById(R.id.txt_letovi_broj_ljudi);

                            txtBrojLeta.setText(letoviItem.getString("br_let"));
                            if(!letoviItem.getString("poletanje").equals(Constants.NULL_RESPONSE)) {
                                JSONObject poletanje = new JSONObject(letoviItem.getString("poletanje"));
                                txtPoletanje.setText(poletanje.getString("date") + ", " + poletanje.getString("time"));
                            } else {
                                txtPoletanje.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!letoviItem.getString("sletanje").equals(Constants.NULL_RESPONSE)) {
                                JSONObject sletanje = new JSONObject(letoviItem.getString("sletanje"));
                                txtSletanje.setText(sletanje.getString("date") + ", " + sletanje.getString("time"));
                            } else {
                                txtSletanje.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            txtPoletanjeIz.setText(letoviItem.getString("od"));
                            txtSletanjeU.setText(letoviItem.getString("do"));
                            txtBrojLjudi.setText(letoviItem.getString("br_men"));

                            ImageView btnShare = (ImageView) customView.findViewById(R.id.btn_share_muzicar_let);

                            btnShare.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ShareContent.share(context, "Broj leta: " + txtBrojLeta.getText() + "\n" + "Poletanje: " + txtPoletanje.getText() + "\n" + "Iz: " + txtPoletanjeIz.getText() + "\n" + "Sletanje: " + txtSletanje.getText() + "\n" + "U: " + txtSletanjeU.getText() + "\n" + "Broj ljudi: " + txtBrojLjudi.getText());
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return customView;
                    }
                });
            } else {
                letoviCarousel.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listSubitem.get(this.listItem.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listItem.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listItem.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final String item = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exp_list_muzicar_single_letovi_item, null);
        }

        TextView txtItem = (TextView) convertView.findViewById(R.id.txt_exp_list_single_muzicar_letovi_name);
        ImageView arrow = (ImageView) convertView.findViewById(R.id.imgArrow);
        CheckBox cbShareLetovi = (CheckBox) convertView.findViewById(R.id.cbMuzicarSingleLetovi);
        cbShare = cbShareLetovi;

        if(this.visibility) {
            cbShareLetovi.setClickable(true);
            cbShareLetovi.setChecked(false);
            cbShareLetovi.setVisibility(View.VISIBLE);
        } else {
            cbShareLetovi.setClickable(false);
            cbShareLetovi.setChecked(false);
            cbShareLetovi.setVisibility(View.GONE);
        }

        if (isExpanded) {
            arrow.setImageResource(R.drawable.arrow_up);
            txtItem.setTextColor(this.context.getResources().getColor(R.color.colorIndicatorExpanded));
        } else {
            arrow.setImageResource(R.drawable.arrow_down);
            txtItem.setTextColor(this.context.getResources().getColor(R.color.colorIndicator));
        }

        txtItem.setText(item);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
