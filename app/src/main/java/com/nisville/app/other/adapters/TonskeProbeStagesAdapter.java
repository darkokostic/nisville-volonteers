package com.nisville.app.other.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.fragments.saradnik.TonskeProbeActivity;
import com.nisville.app.other.models.Stage;

public class TonskeProbeStagesAdapter extends RecyclerView.Adapter<TonskeProbeStagesAdapter.StageViewHolder> {

    private Context context;
    private ArrayList<Stage> data;
    private LayoutInflater inflater;

    public TonskeProbeStagesAdapter(Context context, ArrayList<Stage> data) {
        this.context = context;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public TonskeProbeStagesAdapter.StageViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.card_item_nastupi, parent, false);
        TonskeProbeStagesAdapter.StageViewHolder holder = new TonskeProbeStagesAdapter.StageViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(TonskeProbeStagesAdapter.StageViewHolder holder, final int position) {

        holder.txtIme.setText(data.get(position).getIme());

        holder.btnVidiVise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context.getApplicationContext(), TonskeProbeActivity.class);
                i.putExtra("listaStage", data);
                i.putExtra("idStage", position);
                context.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class StageViewHolder extends RecyclerView.ViewHolder {

        public TextView txtIme;
        public Button btnVidiVise;

        public StageViewHolder(View itemView) {
            super(itemView);
            txtIme = (TextView) itemView.findViewById(R.id.txtSaradnikNastupIme);
            btnVidiVise = (Button) itemView.findViewById(R.id.btnNastupVidiVise);
        }
    }
}
