package com.nisville.app.other.models;


import java.util.ArrayList;
import java.util.HashMap;

public class PressKonferencija {

    private ArrayList<String> listaDatuma;
    private HashMap<String, ArrayList<ArrayList<String>>> listaKonferencija;

    public ArrayList<String> getListaDatuma() {
        return listaDatuma;
    }

    public void setListaDatuma(ArrayList<String> listaDatuma) {
        this.listaDatuma = listaDatuma;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getListaKonferencija() {
        return listaKonferencija;
    }

    public void setListaKonferencija(HashMap<String, ArrayList<ArrayList<String>>> listaKonferencija) {
        this.listaKonferencija = listaKonferencija;
    }
}
