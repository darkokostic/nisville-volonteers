package com.nisville.app.other.models;

import java.util.ArrayList;
import java.util.HashMap;

public class Radionica {

    private ArrayList<String> listaDatuma;
    private HashMap<String, ArrayList<ArrayList<String>>> listaRadionica;

    public ArrayList<String> getListaDatuma() {
        return listaDatuma;
    }

    public void setListaDatuma(ArrayList<String> listaDatuma) {
        this.listaDatuma = listaDatuma;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getListaRadionica() {
        return listaRadionica;
    }

    public void setListaRadionica(HashMap<String, ArrayList<ArrayList<String>>> listaRadionica) {
        this.listaRadionica = listaRadionica;
    }
}
