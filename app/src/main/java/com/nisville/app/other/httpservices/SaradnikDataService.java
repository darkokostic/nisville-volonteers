package com.nisville.app.other.httpservices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.nisville.app.activitiy.LoginActivity;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;

public class SaradnikDataService implements Runnable {

    public downloadComplete caller;

    public interface downloadComplete {
        public void getSaradnikData(String data);
    }

    public SaradnikDataService(downloadComplete caller) {
        this.caller = caller;
    }

    private String link;
    private Activity parent;
    private ProgressDialog overlay;
    private String token;

    public void downloadDataFromLink(String localData, int id, String token, Activity parent) {
        switch(localData) {
            case Constants.SARADNIK_FULL_SYNC_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_FULL_SYNC_API;
                break;
            case Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_MUZICARI_LISTA_API + "/" + LocalStorage.getSaradnikMuzicariSyncTime(parent);
                break;
            case Constants.SARADNIK_STAGES_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_STAGES_LISTA_API + "/" + LocalStorage.getStegesSyncTime(parent);
                break;
            case Constants.SARADNIK_NASTUPI_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_NASTUPI_LISTA_API + "/" + id + "/" + LocalStorage.getSaradnikNastupiSyncTime(parent);
                break;
            case Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_TONSKE_PROBE_LISTA_API + "/" + id + "/" + LocalStorage.getSaradnikTonskeProbeSyncTime(parent);
                break;
            case Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_PRESS_LISTA_API + "/" + LocalStorage.getSaradnikPressSyncTime(parent);
                break;
            case Constants.SARADNIK_WORKSHOP_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_WORKSHOP_LISTA_API + "/" + LocalStorage.getSaradnikWorkshopSyncTime(parent);
                break;
            case Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_PREVOZ_LISTA_API + "/" + LocalStorage.getSaradnikPrevozSyncTime(parent);
                break;
            case Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA:
                this.link = Constants.SARADNIK_NOCENJA_LISTA_API + "/" + LocalStorage.getSaradnikNocenjaSyncTime(parent);
                break;
            case Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA:
                this.link = Constants.MUZICAR_SINGLE_API + "/" + id + "/" + LocalStorage.getMuzicarSinglehSaradnikSyncTime(parent);
                break;
        }

        this.parent = parent;
        this.overlay = new ProgressDialog(parent);
        this.overlay.setMessage(Constants.PLEASE_WAIT);
        this.overlay.setCancelable(false);
        this.token = token;
        Thread t = new Thread(this);
        t.start();
    }

    public void run() {
        threadMsg(download(this.link, this.token, this.parent, this.overlay));
    }

    private void threadMsg(String msg) {
        if (!msg.equals(null) && !msg.equals("")) {
            Message msgObj = handler.obtainMessage();
            Bundle b = new Bundle();
            b.putString("message", msg);
            msgObj.setData(b);
            handler.sendMessage(msgObj);
        }
    }


    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String Response = msg.getData().getString("message");
            caller.getSaradnikData(Response);
        }
    };

    public static String download(String url, String token, final Activity parent, final ProgressDialog overlay) {
        URL website;
        StringBuilder response = null;
        int code = 0;
        String returnData = "";

        parent.runOnUiThread(new Runnable() {
            public void run() {
                overlay.show();
            }
        });

        try {
            website = new URL(url + "?token=" + token);
            HttpURLConnection connection = (HttpURLConnection) website.openConnection();
            String bearerAuth = "Bearer " + token;
            connection.setRequestProperty ("Authorization", bearerAuth);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            code = connection.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();

            if(code == 200) {
                try {
                    final JSONObject data = new JSONObject(response.toString());
                    final String outcome = data.getString(Constants.OUTCOME);
                    if(outcome.equals(Constants.OUTCOME_SUCCESS)) {
                        returnData = data.getString(Constants.RESULT);
                        parent.runOnUiThread(new Runnable() {
                            public void run() {
                                overlay.dismiss();
                            }
                        });
                    } else if(outcome.equals(Constants.OUTCOME_FAILED)) {
                        String error =  data.getString(Constants.ERROR);
                        if(error.equals(Constants.OUTCOME_USER_INACTIVE)) {
                            parent.runOnUiThread(new Runnable() {
                                public void run() {
                                    overlay.dismiss();
                                    Toast.makeText(parent, Constants.USER_INACTIVE_MESSAGE, Toast.LENGTH_SHORT).show();
                                }
                            });
                            LocalStorage.removeToken(parent);
                            Intent i = new Intent(parent, LoginActivity.class);
                            parent.startActivity(i);
                            parent.finish();
                        } else if(error.equals(Constants.OUTCOME_TOKEN_EXPIRED) || error.equals(Constants.OUTCOME_TOKEN_INVALID)) {
                            parent.runOnUiThread(new Runnable() {
                                public void run() {
                                    overlay.dismiss();
                                    Toast.makeText(parent, Constants.TOKEN_EXPIRED_MESSAGE, Toast.LENGTH_SHORT).show();
                                }
                            });
                            LocalStorage.removeToken(parent);
                            Intent i = new Intent(parent, LoginActivity.class);
                            parent.startActivity(i);
                            parent.finish();
                        } else if(error.equals(Constants.OUTCOME_USER_NOT_FOUND)) {
                            parent.runOnUiThread(new Runnable() {
                                public void run() {
                                    overlay.dismiss();
                                    Toast.makeText(parent, Constants.USER_NOT_FOUND_MESSAGE, Toast.LENGTH_SHORT).show();
                                }
                            });
                            LocalStorage.removeToken(parent);
                            Intent i = new Intent(parent, LoginActivity.class);
                            parent.startActivity(i);
                            parent.finish();
                        }
                    } else {
                        parent.runOnUiThread(new Runnable() {
                            public void run() {
                                overlay.dismiss();
                                Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (final JSONException e) {
                    parent.runOnUiThread(new Runnable() {
                        public void run() {
                            overlay.dismiss();
                            Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else {
                parent.runOnUiThread(new Runnable() {
                    public void run() {
                        overlay.dismiss();
                        Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (final Exception  e) {
            parent.runOnUiThread(new Runnable() {
                public void run() {
                    overlay.dismiss();
                    Toast.makeText(parent, Constants.SERVER_NOT_RESPONSE, Toast.LENGTH_SHORT).show();
                }
            });
        }
        return returnData;
    }
}

