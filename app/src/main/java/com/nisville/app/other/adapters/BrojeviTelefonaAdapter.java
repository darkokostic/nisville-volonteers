package com.nisville.app.other.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import com.nisville.app.R;
import com.nisville.app.other.helpers.CallNumber;
import com.nisville.app.other.helpers.SMSSend;
import com.nisville.app.other.helpers.ShareContent;
import com.nisville.app.other.models.Volonter;

public class BrojeviTelefonaAdapter extends RecyclerView.Adapter<BrojeviTelefonaAdapter.BrojeviTelefonaViewHolder> {

    private Context context;
    private ArrayList<Volonter> data;
    private LayoutInflater inflater;

    public BrojeviTelefonaAdapter(Context context, ArrayList<Volonter> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public BrojeviTelefonaViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.card_item_brojevi_telefona, parent, false);
        BrojeviTelefonaViewHolder holder = new BrojeviTelefonaViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(BrojeviTelefonaViewHolder holder, final int position) {
        holder.txtIme.setText(data.get(position).getIme());
        holder.txtBrojTelefona.setText(data.get(position).getBrojTelefona());
        holder.txtKategorija.setText(data.get(position).getKategorija());

        final Volonter infoData = data.get(position);

        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CallNumber.call(context, infoData.getBrojTelefona());
            }
        });

        holder.btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SMSSend.send(context, infoData.getBrojTelefona());
            }
        });

        holder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareContent.share(context, infoData.getKategorija() + ": " + "\n" + infoData.getIme() + ", " + "\n" + infoData.getBrojTelefona());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class BrojeviTelefonaViewHolder extends RecyclerView.ViewHolder {

        public TextView txtIme;
        public TextView txtBrojTelefona;
        public TextView txtKategorija;
        public ImageView btnCall;
        public ImageView btnMessage;
        public ImageView btnShare;

        public BrojeviTelefonaViewHolder(View itemView) {
            super(itemView);

            txtIme = (TextView) itemView.findViewById(R.id.txtHostBrojeviIme);
            txtBrojTelefona = (TextView) itemView.findViewById(R.id.txtHostBrojeviBrojTelefona);
            txtKategorija = (TextView) itemView.findViewById(R.id.txtHostBrojeviKategorija);
            btnCall = (ImageView) itemView.findViewById(R.id.btnHotBrojeviTelefonaCall);
            btnMessage = (ImageView) itemView.findViewById(R.id.btnHostBrojeviTelefonaMessage);
            btnShare = (ImageView) itemView.findViewById(R.id.btnHostBrojeviTelefonaShare);
        }
    }
}
