package com.nisville.app.other.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.fragments.host.MuzicarHostActivity;
import com.nisville.app.other.models.Muzicar;


public class HostMuzicariAdapter extends RecyclerView.Adapter<HostMuzicariAdapter.HostMuzicariViewHolder> {

    private Context context;
    private ArrayList<Muzicar> data;
    private LayoutInflater inflater;

    public HostMuzicariAdapter(Context context, ArrayList<Muzicar> data) {
        this.context = context;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public HostMuzicariAdapter.HostMuzicariViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.card_item_muzicari, parent, false);
        HostMuzicariViewHolder holder = new HostMuzicariViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(HostMuzicariAdapter.HostMuzicariViewHolder holder, final int position) {

        holder.txtIme.setText(data.get(position).getIme());
        holder.txtDrzava.setText(data.get(position).getDrzava());
        holder.txtDatum.setText(data.get(position).getDatum());

        final Muzicar infoData = data.get(position);

        holder.btnVidiVise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context.getApplicationContext(), MuzicarHostActivity.class);
                i.putExtra("listaMuzicara", data);
                i.putExtra("idMuzicara", position);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class HostMuzicariViewHolder extends RecyclerView.ViewHolder {

        public TextView txtIme;
        public TextView txtDrzava;
        public TextView txtDatum;
        public Button btnVidiVise;

        public HostMuzicariViewHolder(View itemView) {
            super(itemView);

            txtIme = (TextView) itemView.findViewById(R.id.txtMuzicariIme);
            txtDrzava = (TextView) itemView.findViewById(R.id.txtMuzicariDrzava);
            txtDatum = (TextView) itemView.findViewById(R.id.txtMuzicariDatum);
            btnVidiVise = (Button) itemView.findViewById(R.id.btnMuzicariVidiVise);
        }
    }
}
