package com.nisville.app.other.models;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class TonskaProba implements Serializable {

    private ArrayList<String> listaDatuma;
    private HashMap<String, ArrayList<ArrayList<String>>> listaTonskihProba;

    public ArrayList<String> getListaDatuma() {
        return listaDatuma;
    }

    public void setListaDatuma(ArrayList<String> listaDatuma) {
        this.listaDatuma = listaDatuma;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getListaTonskihProba() {
        return listaTonskihProba;
    }

    public void setListaTonskihProba(HashMap<String, ArrayList<ArrayList<String>>> listaTonskihProba) {
        this.listaTonskihProba = listaTonskihProba;
    }
}
