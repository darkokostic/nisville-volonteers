package com.nisville.app.other.adapters.muzicarsingleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nisville.app.other.helpers.ShareContent;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import com.nisville.app.R;
import com.nisville.app.other.helpers.Constants;

public class NastupiSliderAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listItem;
    private HashMap<String, List<String>> listSubitem;
    private boolean visibility;
    private CheckBox cbShare;

    public NastupiSliderAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
        this.context = context;
        this.listItem = listDataHeader;
        this.listSubitem = listChildData;
    }

    public void setCheckBoxVisibility(boolean visibility) {
        this.visibility = visibility;
        notifyDataSetChanged();
    }

    public boolean getIsShareable() {
        return cbShare.isChecked();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listSubitem.get(this.listItem.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String nastupiChild = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_list_muzicar_single_nastupi_subitem, null);
        }

        try {
            CarouselView nastupiCarousel = (CarouselView) convertView.findViewById(R.id.nastupiCarouselView);
            final JSONArray sliderData = new JSONArray(nastupiChild);
            if(sliderData.length() > 0) {
                nastupiCarousel.setPageCount(sliderData.length());
                nastupiCarousel.setViewListener(new ViewListener() {
                    @Override
                    public View setViewForPosition(int position) {
                        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View customView = infalInflater.inflate(R.layout.nastupi_muzicar_single_swipe_slider, null);
                        try {
                            JSONObject nastupItem = new JSONObject(sliderData.get(position).toString());
                            final TextView txtStage = (TextView) customView.findViewById(R.id.txt_stage);
                            final TextView txtPocetak = (TextView) customView.findViewById(R.id.txt_pocetak);
                            final TextView txtZavrsetak = (TextView) customView.findViewById(R.id.txt_zavrsetak);
                            final TextView txtTrajanje = (TextView) customView.findViewById(R.id.txt_trajanje);
                            final TextView txtTonska = (TextView) customView.findViewById(R.id.txt_tonska_proba);
                            JSONObject stage = new JSONObject(nastupItem.getString("stage"));
                            if(!nastupItem.getString("vreme").equals(Constants.NIJE_UNETO_LABEL)) {
                                JSONObject vreme = new JSONObject(nastupItem.getString("vreme"));
                                txtPocetak.setText(vreme.getString("date") + " u " + vreme.getString("time"));
                            } else {
                                txtPocetak.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!nastupItem.getString("zavrsetak").equals(Constants.NULL_RESPONSE)) {
                                JSONObject zavrsetak = new JSONObject(nastupItem.getString("zavrsetak"));
                                txtZavrsetak.setText(zavrsetak.getString("date") + " u " + zavrsetak.getString("time"));
                            } else {
                                txtZavrsetak.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!nastupItem.getString("soundcheck").equals(Constants.NIJE_UNETO_LABEL)) {
                                JSONObject tonska = new JSONObject(nastupItem.getString("soundcheck"));
                                txtTonska.setText(tonska.getString("date") + " u " + tonska.getString("time"));
                            } else {
                                txtTonska.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            if(!nastupItem.getString("trajanje").equals(Constants.NULL_RESPONSE)) {
                                txtTrajanje.setText(nastupItem.getString("trajanje"));
                            } else {
                                txtTrajanje.setText(Constants.NIJE_UNETO_LABEL);
                            }
                            txtStage.setText(stage.getString("ime"));

                            ImageView btnShare = (ImageView) customView.findViewById(R.id.btn_share_muzicar_nastup);

                            btnShare.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ShareContent.share(context, "Stage: " + txtStage.getText() + "\n" + "Početak: " + txtPocetak.getText() + "\n" + "Završetak: " + txtZavrsetak.getText() + "\n" + "Trajanje: " + txtTrajanje.getText() + "\n" + "Tonska proba: " + txtTonska.getText());
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return customView;
                    }
                });
            } else {
                nastupiCarousel.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listSubitem.get(this.listItem.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listItem.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listItem.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {
        final String item = (String) getGroup(groupPosition);

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exp_list_muzicar_single_nastupi_item, null);
        }

        TextView txtItem = (TextView) convertView.findViewById(R.id.txt_exp_list_single_muzicar_nastupi_name);
        ImageView arrow = (ImageView) convertView.findViewById(R.id.imgArrow);
        CheckBox cbShareNastupi = (CheckBox) convertView.findViewById(R.id.cbMuzicarSingleNastupi);
        cbShare = cbShareNastupi;

        if(this.visibility) {
            cbShareNastupi.setClickable(true);
            cbShareNastupi.setChecked(false);
            cbShareNastupi.setVisibility(View.VISIBLE);
        } else {
            cbShareNastupi.setClickable(false);
            cbShareNastupi.setChecked(false);
            cbShareNastupi.setVisibility(View.GONE);
        }

        if(isExpanded) {
            arrow.setImageResource(R.drawable.arrow_up);
            txtItem.setTextColor(this.context.getResources().getColor(R.color.colorIndicatorExpanded));
        } else {
            arrow.setImageResource(R.drawable.arrow_down);
            txtItem.setTextColor(this.context.getResources().getColor(R.color.colorIndicator));
        }

        txtItem.setText(item);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
