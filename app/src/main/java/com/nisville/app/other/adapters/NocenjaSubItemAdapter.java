package com.nisville.app.other.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.other.helpers.ShareContent;
import com.nisville.app.other.models.Nocenje;

public class NocenjaSubItemAdapter extends RecyclerView.Adapter<NocenjaSubItemAdapter.NocenjaSubItemViewHolder> {

    private Context context;
    private ArrayList<Nocenje> data;
    private LayoutInflater inflater;

    public NocenjaSubItemAdapter(Context context, ArrayList<Nocenje> data) {
        this.context = context;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public NocenjaSubItemAdapter.NocenjaSubItemViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.exp_list_nocenja_subitem_card, parent, false);
        NocenjaSubItemAdapter.NocenjaSubItemViewHolder holder = new NocenjaSubItemAdapter.NocenjaSubItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(NocenjaSubItemAdapter.NocenjaSubItemViewHolder holder, final int position) {

        holder.txtMusician.setText(data.get(position).getMusician());
        holder.txtBrojClanova.setText(data.get(position).getBrojClanova() + "");
        holder.txtDatumPocetka.setText(data.get(position).getPocetak());
        holder.txtDatumKraja.setText(data.get(position).getKraj());
        holder.txtNazivHotela.setText(data.get(position).getAccomodation());
        holder.txtSoba.setText(data.get(position).getSoba());

        final Nocenje infoData = data.get(position);

        holder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareContent.share(context, "Noćenje: " + "\nMuzičar: " + infoData.getMusician() + "\nBroj članova: " + infoData.getBrojClanova() + ", " + "\nOd: " + infoData.getPocetak() + ", " + "\nDo: " + infoData.getKraj() + ", " + "\nSmeštaj: " + infoData.getAccomodation() + ", " + "\nSoba: " + infoData.getSoba());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class NocenjaSubItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtMusician;
        public TextView txtBrojClanova;
        public TextView txtDatumPocetka;
        public TextView txtDatumKraja;
        public TextView txtNazivHotela;
        public TextView txtSoba;
        public ImageView btnShare;

        public NocenjaSubItemViewHolder(View itemView) {
            super(itemView);

            txtMusician = (TextView) itemView.findViewById(R.id.txtNocenjaSubitemImeMuzicara);
            txtBrojClanova = (TextView) itemView.findViewById(R.id.txtNocenjaSubitemBrojClanova);
            txtDatumPocetka = (TextView) itemView.findViewById(R.id.txtNocenjaSubitemDatumPocetka);
            txtDatumKraja = (TextView) itemView.findViewById(R.id.txtNocenjaSubitemDatumKraja);
            txtNazivHotela = (TextView) itemView.findViewById(R.id.txtNocenjaSubitemAccomodation);
            txtSoba = (TextView) itemView.findViewById(R.id.txtNocenjaSubitemSoba);
            btnShare = (ImageView) itemView.findViewById(R.id.btn_share_nocenje);
        }
    }
}
