package com.nisville.app.other.helpers;

import android.content.Context;
import android.content.Intent;

public class ShareContent {
    public static void share(Context context, String content) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, content);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }
}
