package com.nisville.app.other.helpers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

public class CallNumber {
    public static void call(Context context, String number) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + number));
            context.startActivity(intent);
        } catch (SecurityException e) {
            Toast.makeText(context, Constants.NO_CALL_ACTIVITY_MESSAGE, Toast.LENGTH_SHORT).show();
        }
    }
}
