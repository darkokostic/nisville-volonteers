package com.nisville.app.fragments.saradnik;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.SaradnikDataService;
import com.nisville.app.other.adapters.ExpListPrevoziAdapter;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.models.Prevoz;

public class Prevozi extends Fragment implements SaradnikDataService.downloadComplete {

    public Prevozi() {
        // Required empty public constructor
    }

    private ExpListPrevoziAdapter adapter;
    private ExpandableListView listView;
    private Context context;
    private MaterialRefreshLayout materialRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_saradnik_prevozi, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        getActivity().setTitle("Prevozi");

        context = this.getContext();
        materialRefreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.prevoziRefresh);
        int refreshColors[] = {getResources().getColor(R.color.colorPrimary)};
        materialRefreshLayout.setProgressColors(refreshColors);
        listView = (ExpandableListView) view.findViewById(R.id.expListViewPrevozi);

        setDataOnView();

        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
            if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                    SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                    saradnikDataService.downloadDataFromLink(Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                } else {
                    materialRefreshLayout.finishRefresh();
                }
            } else {
                SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                saradnikDataService.downloadDataFromLink(Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
            }
        } else {
            setDataOnView();
        }

        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                // CHECK INTERNET CONNECTION AND FIRE REQUEST
                if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                        if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                            SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) Prevozi.this);
                            saradnikDataService.downloadDataFromLink(Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                        }
                    } else {
                        SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) Prevozi.this);
                        saradnikDataService.downloadDataFromLink(Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                    }
                } else {
                    setDataOnView();
                }
            }
        });
        return view;
    }

    @Override
    public void getSaradnikData(String data) {
        try {
            JSONObject result = new JSONObject(data);

            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA, result.getString(Constants.DATA_JSON));
                LocalStorage.setSaradnikPrevozSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView();
            } else {
                materialRefreshLayout.finishRefresh();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setDataOnView () {
        materialRefreshLayout.finishRefresh();
        String prevozi = LocalStorage.getLocalData(context, Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA);
        try {
            JSONArray jsonArrayPrevozi = new JSONArray(prevozi);

            ArrayList<String> listaDatuma = new ArrayList<String>();
            HashMap<String, ArrayList<ArrayList<Prevoz>>> listaPrevoza = new HashMap<String, ArrayList<ArrayList<Prevoz>>>();

            for (int i = 0 ; i < jsonArrayPrevozi.length() ; i++) {
                JSONObject currentDate = new JSONObject(jsonArrayPrevozi.get(i).toString());
                listaDatuma.add(currentDate.getString("date"));
            }

            for(int i = 0; i < listaDatuma.size(); i++) {
                JSONObject currentDate = new JSONObject(jsonArrayPrevozi.get(i).toString());
                JSONArray transportsArray = new JSONArray(currentDate.getString("transports"));

                ArrayList<ArrayList<Prevoz>> subItem = new ArrayList<ArrayList<Prevoz>>();
                ArrayList<Prevoz> subItemStrings = new ArrayList<Prevoz>();

                for(int j = 0; j < transportsArray.length(); j++) {
                    JSONObject currentTransport = new JSONObject(transportsArray.get(j).toString());

                    Prevoz prevoz = new Prevoz();

                    if(!currentTransport.getString("muzicar").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setImeMuzicara(currentTransport.getString("muzicar"));
                    } else {
                        prevoz.setImeMuzicara(Constants.NIJE_UNETO_LABEL);
                    }
                    if(!currentTransport.getString("ucesnik").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setUcesnik(currentTransport.getString("ucesnik"));
                    } else {
                        prevoz.setUcesnik(Constants.NIJE_UNETO_LABEL);
                    }
                    if(!currentTransport.getString("broj_clanova").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setBrojClanova(currentTransport.getInt("broj_clanova"));
                    } else {
                        prevoz.setBrojClanova(0);
                    }
                    if(!currentTransport.getString("polazak_mesto").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setPolazakMesto(currentTransport.getString("polazak_mesto"));
                    } else {
                        prevoz.setPolazakMesto(Constants.NIJE_UNETO_LABEL);
                    }
                    if(!currentTransport.getString("polazak_dan").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setPolazakDatum(currentTransport.getString("polazak_dan") + ", ");
                    } else {
                        prevoz.setPolazakDatum(Constants.NIJE_UNETO_LABEL + ", ");
                    }
                    if(!currentTransport.getString("polazak_vreme").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setPolazakVreme(currentTransport.getString("polazak_vreme"));
                    } else {
                        prevoz.setPolazakVreme(Constants.NIJE_UNETO_LABEL);
                    }
                    if(!currentTransport.getString("dolazak_mesto").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setDolazakMesto(currentTransport.getString("dolazak_mesto"));
                    } else {
                        prevoz.setDolazakMesto(Constants.NIJE_UNETO_LABEL);
                    }
                    if(!currentTransport.getString("dolazak_dan").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setDolazakDatum(currentTransport.getString("dolazak_dan") + ", ");
                    } else {
                        prevoz.setDolazakDatum(Constants.NIJE_UNETO_LABEL + ", ");
                    }
                    if(!currentTransport.getString("dolazak_vreme").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setDolazakVreme(currentTransport.getString("dolazak_vreme"));
                    } else {
                        prevoz.setDolazakVreme(Constants.NIJE_UNETO_LABEL);
                    }
                    if(!currentTransport.getString("vozac").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setVozac(currentTransport.getString("vozac"));
                    } else {
                        prevoz.setVozac(Constants.NIJE_UNETO_LABEL);
                    }
                    if(!currentTransport.getString("vozilo").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setVozilo(currentTransport.getString("vozilo"));
                    } else {
                        prevoz.setVozilo(Constants.NIJE_UNETO_LABEL);
                    }
                    if(!currentTransport.getString("host").equals(Constants.NULL_RESPONSE)) {
                        prevoz.setHost(currentTransport.getString("host"));
                    } else {
                        prevoz.setHost(Constants.NIJE_UNETO_LABEL);
                    }

                    subItemStrings.add(prevoz);
                }
                subItem.add(subItemStrings);
                listaPrevoza.put(listaDatuma.get(i), subItem);
            }
            adapter = new ExpListPrevoziAdapter(context, listaDatuma, listaPrevoza);
            listView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
