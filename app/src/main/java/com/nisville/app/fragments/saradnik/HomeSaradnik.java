package com.nisville.app.fragments.saradnik;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nisville.app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeSaradnik extends Fragment {

    public HomeSaradnik() {
        // Required empty public constructor
    }

    public CardView cardMuzicari;
    public CardView cardTonskeProbe;
    public CardView cardNastupi;
    public CardView cardPres;
    public CardView cardPrevozi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_saradnik_home, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        SharedPreferences activityPref = this.getActivity().getSharedPreferences("ActivityInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = activityPref.edit();
        edit.putString("activity", "home");
        edit.apply();

        cardMuzicari = (CardView) view.findViewById(R.id.cardSaradnikMuzicari);
        cardTonskeProbe = (CardView) view.findViewById(R.id.cardSaradnikTonskeProbe);
        cardNastupi = (CardView) view.findViewById(R.id.cardSaradnikNastupi);
        cardPres = (CardView) view.findViewById(R.id.cardSaradnikPres);
        cardPrevozi = (CardView) view.findViewById(R.id.cardSaradnikPrevozi);

        cardMuzicari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationView navView = (NavigationView) getActivity().findViewById(R.id.nav_view);
                navView.getMenu().getItem(1).setChecked(true);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                MuzicariSaradnik muzicariSaradnik = new MuzicariSaradnik();
                fragmentTransaction.replace(R.id.saradnik_layout, muzicariSaradnik);
                fragmentTransaction.commit();
            }
        });

        cardTonskeProbe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationView navView = (NavigationView) getActivity().findViewById(R.id.nav_view);
                navView.getMenu().getItem(2).setChecked(true);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                StagesTonskeProbe stagesTonskeProbe = new StagesTonskeProbe();
                fragmentTransaction.replace(R.id.saradnik_layout, stagesTonskeProbe);
                fragmentTransaction.commit();
            }
        });

        cardNastupi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationView navView = (NavigationView) getActivity().findViewById(R.id.nav_view);
                navView.getMenu().getItem(3).setChecked(true);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                StagesNastupi stagesNastupi = new StagesNastupi();
                fragmentTransaction.replace(R.id.saradnik_layout, stagesNastupi);
                fragmentTransaction.commit();
            }
        });

        cardPres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationView navView = (NavigationView) getActivity().findViewById(R.id.nav_view);
                navView.getMenu().getItem(4).setChecked(true);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                PressKonferencije pressKonferencije = new PressKonferencije();
                fragmentTransaction.replace(R.id.saradnik_layout, pressKonferencije);
                fragmentTransaction.commit();
            }
        });

        cardPrevozi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationView navView = (NavigationView) getActivity().findViewById(R.id.nav_view);
                navView.getMenu().getItem(5).setChecked(true);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                Prevozi prevozi = new Prevozi();
                fragmentTransaction.replace(R.id.saradnik_layout, prevozi);
                fragmentTransaction.commit();
            }
        });

        return view;
    }
}
