package com.nisville.app.fragments.host;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.HostDataService;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.models.Volonter;
import com.nisville.app.other.adapters.BrojeviTelefonaAdapter;


public class StageMenadzeriTab extends Fragment implements HostDataService.downloadComplete {

    public StageMenadzeriTab() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private RecyclerView rv;
    private Context context;
    private MaterialRefreshLayout materialRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_host_stage_menadzeri, container, false);

        context = this.getContext();
        materialRefreshLayout = (MaterialRefreshLayout) rootView.findViewById(R.id.menadzeriRefresh);
        int refreshColors[] = {getResources().getColor(R.color.colorPrimary)};
        materialRefreshLayout.setProgressColors(refreshColors);
        rv = (RecyclerView) rootView.findViewById(R.id.host_stage_menadzeri_recyclerview);

        setDataOnView();

        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
            if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                    HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) this);
                    hostDataService.downloadDataFromLink(Constants.HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                }
            } else {
                HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) this);
                hostDataService.downloadDataFromLink(Constants.HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
            }
        } else {
            setDataOnView();
        }

        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                // CHECK INTERNET CONNECTION AND FIRE REQUEST
                if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                        if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                            HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) StageMenadzeriTab.this);
                            hostDataService.downloadDataFromLink(Constants.HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                        } else {
                            materialRefreshLayout.finishRefresh();
                        }
                    } else {
                        HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) StageMenadzeriTab.this);
                        hostDataService.downloadDataFromLink(Constants.HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                    }
                } else {
                    setDataOnView();
                }
            }
        });
        return rootView;
    }

    @Override
    public void getData(String data) {
        try {
            JSONObject result = new JSONObject(data);
            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA, result.getString(Constants.DATA_JSON));
                LocalStorage.setHostBrojeviTelefonaSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView();
            } else {
                materialRefreshLayout.finishRefresh();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setDataOnView() {
        materialRefreshLayout.finishRefresh();
        String brojeviTelefona = LocalStorage.getLocalData(context, Constants.HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA);

        try {
            JSONObject brojeviObj = new JSONObject(brojeviTelefona);

            JSONArray jsonArrayStageMenadzeri = new JSONArray(brojeviObj.getString("stageman"));
            ArrayList<Volonter> stageMenadzeri = new ArrayList<>();

            for (int i = 0 ; i < jsonArrayStageMenadzeri.length() ; i++) {
                JSONObject currentStageMenadzer = new JSONObject(jsonArrayStageMenadzeri.get(i).toString());
                Volonter noviStageMenadzer = new Volonter();

                noviStageMenadzer.setIme(currentStageMenadzer.getString("name"));
                noviStageMenadzer.setBrojTelefona(currentStageMenadzer.getString("broj_telefona"));
                noviStageMenadzer.setKategorija("Stage Menadzer");
                stageMenadzeri.add(noviStageMenadzer);
            }

            rv.setHasFixedSize(true);
            BrojeviTelefonaAdapter adapter = new BrojeviTelefonaAdapter(getContext(), stageMenadzeri);
            rv.setAdapter(adapter);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            rv.setLayoutManager(llm);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
