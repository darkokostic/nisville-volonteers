package com.nisville.app.fragments.host;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.HostDataService;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;

public class HomeHost extends Fragment implements HostDataService.downloadComplete {

    public HomeHost() {
        // Required empty public constructor
    }

    private TextView txtPodsetnik;
    private TextView txtNapomena;
    private Context context;
    private CarouselView podsetnikCarouselView;
    private CarouselView napomeneCarouselView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_host_home, container, false);

        getActivity().setTitle("Početna");

        podsetnikCarouselView = (CarouselView) view.findViewById(R.id.podsetnikCarouselView);
        napomeneCarouselView = (CarouselView) view.findViewById(R.id.napomeneCarouselView);

        SharedPreferences activityPref = getActivity().getSharedPreferences("ActivityInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = activityPref.edit();
        edit.putString("activity", "home");
        edit.apply();

        context = this.getContext();
        setDataOnView();

        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
            if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                    HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) this);
                    hostDataService.downloadDataFromLink(Constants.HOST_POCETNA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                }
            } else {
                HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) this);
                hostDataService.downloadDataFromLink(Constants.HOST_POCETNA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
            }
        } else {
            setDataOnView();
        }
        return view;
    }

    @Override
    public void getData(String data) {
        try {
            JSONObject result = new JSONObject(data);
            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.HOST_POCETNA_LOCAL_STORAGE_DATA, result.getString(Constants.DATA_JSON));
                LocalStorage.setHostPocetnaSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setDataOnView() {
        try {
            JSONObject localData = new JSONObject(LocalStorage.getLocalData(context, Constants.HOST_POCETNA_LOCAL_STORAGE_DATA));
            String napomena = "";
            String podsetnik = "";
            if(localData.has("napomena")) {
                napomena = localData.getString("napomena");
                final JSONArray napomenaArray = new JSONArray(napomena);
                napomeneCarouselView.setPageCount(napomenaArray.length());
                napomeneCarouselView.setViewListener(new ViewListener() {
                    @Override
                    public View setViewForPosition(int position) {
                        View customView = getActivity().getLayoutInflater().inflate(R.layout.host_home_swipe_slider, null);
                        try {
                            JSONObject napomenaItem = new JSONObject(napomenaArray.get(position).toString());
                            txtNapomena = (TextView) customView.findViewById(R.id.txt_objava);
                            txtNapomena.setText(napomenaItem.getString("objava"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return customView;
                    }
                });
            }

            if(localData.has("podsetnik")) {
                podsetnik = localData.getString("podsetnik");
                final JSONArray podsetnikArray = new JSONArray(podsetnik);
                podsetnikCarouselView.setPageCount(podsetnikArray.length());
                podsetnikCarouselView.setViewListener(new ViewListener() {
                    @Override
                    public View setViewForPosition(int position) {
                        View customView = getActivity().getLayoutInflater().inflate(R.layout.host_home_swipe_slider, null);
                        try {
                            JSONObject podsetnikItem = new JSONObject(podsetnikArray.get(position).toString());
                            txtPodsetnik = (TextView) customView.findViewById(R.id.txt_objava);
                            txtPodsetnik.setText(podsetnikItem.getString("objava"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return customView;
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
