package com.nisville.app.fragments.host;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import android.widget.TextView;

import com.jaeger.library.StatusBarUtil;

import com.nisville.app.R;

public class BrojeviTelefonaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host_brojevi_telefona);
        StatusBarUtil.setColor(BrojeviTelefonaActivity.this, getResources().getColor(R.color.colorPrimary));

        Toolbar toolbar = (Toolbar) findViewById(R.id.host_brojevi_telefona_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Brojevi Telefona");

        ViewPager viewPager = (ViewPager) findViewById(R.id.host_brojevi_telefona_viewpager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), BrojeviTelefonaActivity.this);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.host_brojevi_telefona_tabs);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    class PagerAdapter extends FragmentPagerAdapter {
        String tabTitles[] = new String[] { "TIM LIDERI", "STAGE MENADŽERI", "ORGANIZACIJA", "RESTORANI" };
        Context context;

        public PagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new TimLideriTab();
                case 1:
                    return new StageMenadzeriTab();
                case 2:
                    return new OrganizacijaTab();
                case 3:
                    return new RestoraniTab();
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }

        public View getTabView(int position) {
            View tab;
            TextView tv;
            switch(position) {
                case 0:
                    tab = LayoutInflater.from(BrojeviTelefonaActivity.this).inflate(R.layout.host_tim_lideri_tab, null);
                    tv = (TextView) tab.findViewById(R.id.host_tim_lideri_tab_text);
                    tv.setText(tabTitles[position]);
                    return tab;
                case 1:
                    tab = LayoutInflater.from(BrojeviTelefonaActivity.this).inflate(R.layout.host_stage_menadzeri_tab, null);
                    tv = (TextView) tab.findViewById(R.id.host_stage_menadzeri_tab_text);
                    tv.setText(tabTitles[position]);
                    return tab;
                case 2:
                    tab = LayoutInflater.from(BrojeviTelefonaActivity.this).inflate(R.layout.host_organizacija_tab, null);
                    tv = (TextView) tab.findViewById(R.id.host_organizacija_tab_text);
                    tv.setText(tabTitles[position]);
                    return tab;
                case 3:
                    tab = LayoutInflater.from(BrojeviTelefonaActivity.this).inflate(R.layout.host_restorani_tab, null);
                    tv = (TextView) tab.findViewById(R.id.host_restorani_tab_text);
                    tv.setText(tabTitles[position]);
                    return tab;
            }
            return null;
        }
    }
}
