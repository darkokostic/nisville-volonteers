package com.nisville.app.fragments.host;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.HostDataService;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.models.Muzicar;
import com.nisville.app.other.adapters.HostMuzicariAdapter;


public class MuzicariHost extends Fragment implements HostDataService.downloadComplete {

    public MuzicariHost() {
        // Required empty public constructor
    }

    private RecyclerView rv;
    private Context context;
    private MaterialRefreshLayout materialRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_host_moji_muzicari, container, false);

        getActivity().setTitle("Moji muzičari");

        context = this.getContext();
        materialRefreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.muzicariHostRefresh);
        int refreshColors[] = {getResources().getColor(R.color.colorPrimary)};
        materialRefreshLayout.setProgressColors(refreshColors);
        rv = (RecyclerView) view.findViewById(R.id.host_moji_muzicari_recyclerview);

        setDataOnView();

        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
            if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                    HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) this);
                    hostDataService.downloadDataFromLink(Constants.HOST_MUZICARI_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                }
            } else {
                HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) this);
                hostDataService.downloadDataFromLink(Constants.HOST_MUZICARI_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
            }
        } else {
            setDataOnView();
        }

        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                // CHECK INTERNET CONNECTION AND FIRE REQUEST
                if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                        if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                            HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) MuzicariHost.this);
                            hostDataService.downloadDataFromLink(Constants.HOST_MUZICARI_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                        } else {
                            materialRefreshLayout.finishRefresh();
                        }
                    } else {
                        HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) MuzicariHost.this);
                        hostDataService.downloadDataFromLink(Constants.HOST_MUZICARI_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                    }
                } else {
                    setDataOnView();
                }
            }
        });

        return view;
    }

    @Override
    public void getData(String data) {
        try {
            JSONObject result = new JSONObject(data);
            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.HOST_MUZICARI_LOCAL_STORAGE_DATA, result.getString(Constants.DATA_JSON));
                LocalStorage.setHostMuzicariSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView();
            } else {
                materialRefreshLayout.finishRefresh();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setDataOnView () {
        materialRefreshLayout.finishRefresh();
        String hostMuzicari = LocalStorage.getLocalData(context, Constants.HOST_MUZICARI_LOCAL_STORAGE_DATA);
        try {
            JSONArray jsonArrayMuzicari = new JSONArray(hostMuzicari);
            ArrayList<Muzicar> muzicari = new ArrayList<>();

            for (int i = 0 ; i < jsonArrayMuzicari.length() ; i++) {
                JSONObject currentMuzicar = new JSONObject(jsonArrayMuzicari.get(i).toString());
                Muzicar noviMuzicar = new Muzicar();

                noviMuzicar.setId(currentMuzicar.getInt("id"));
                noviMuzicar.setIme(currentMuzicar.getString("ime"));
                noviMuzicar.setDrzava(currentMuzicar.getString("poreklo"));
                String datum = "";
                if(currentMuzicar.getString("dolazak_datum").equals(Constants.NULL_RESPONSE)) {
                    datum = Constants.NIJE_UNETO_LABEL;
                } else {
                    datum += currentMuzicar.getString("dolazak_datum");
                }
                if(!currentMuzicar.getString("odlazak_datum").equals(Constants.NULL_RESPONSE)) {
                    datum += " - " + currentMuzicar.getString("odlazak_datum");
                }
                noviMuzicar.setDatum(datum);
                muzicari.add(noviMuzicar);
            }

            rv.setHasFixedSize(true);
            HostMuzicariAdapter adapter = new HostMuzicariAdapter(getContext(), muzicari);
            rv.setAdapter(adapter);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            rv.setLayoutManager(llm);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
