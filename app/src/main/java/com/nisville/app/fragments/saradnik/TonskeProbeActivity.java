package com.nisville.app.fragments.saradnik;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.SaradnikDataService;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.models.Stage;
import com.nisville.app.other.adapters.ExpListNoSwipeAdapter;

public class TonskeProbeActivity extends AppCompatActivity implements SaradnikDataService.downloadComplete {

    private ExpListNoSwipeAdapter adapter;
    private ExpandableListView listView;
    private Context context;
    private MaterialRefreshLayout materialRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tonska_proba);
        StatusBarUtil.setColor(TonskeProbeActivity.this, getResources().getColor(R.color.colorPrimary));

        Toolbar toolbar = (Toolbar) findViewById(R.id.saradnik_tonska_proba_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<Stage> data = (ArrayList<Stage>) getIntent().getSerializableExtra("listaStage");
        final Stage stage = data.get(getIntent().getIntExtra("idStage", 0));

        setTitle(stage.getIme());

        context = this;
        materialRefreshLayout = (MaterialRefreshLayout) findViewById(R.id.tonskaRefresh);
        int refreshColors[] = {getResources().getColor(R.color.colorPrimary)};
        materialRefreshLayout.setProgressColors(refreshColors);

        listView = (ExpandableListView) findViewById(R.id.expListViewTonskaProba);

        setDataOnView(stage.getId());

        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if(CheckNetworkConnection.isNetworkAvailable(this)) {
            if(LocalStorage.getToggleButtonState(this).equals("enabled")) {
                if(CheckNetworkConnection.checkConnectionType(this).equals(Constants.INTERNET_TYPE_WIFI)) {
                    SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                    saradnikDataService.downloadDataFromLink(Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA, stage.getId(), LocalStorage.getToken(context), this);
                }
            } else {
                SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                saradnikDataService.downloadDataFromLink(Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA, stage.getId(), LocalStorage.getToken(context), this);
            }
        } else {
            setDataOnView(stage.getId());
        }

        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                // CHECK INTERNET CONNECTION AND FIRE REQUEST
                if(CheckNetworkConnection.isNetworkAvailable(TonskeProbeActivity.this)) {
                    if(LocalStorage.getToggleButtonState(TonskeProbeActivity.this).equals("enabled")) {
                        if(CheckNetworkConnection.checkConnectionType(TonskeProbeActivity.this).equals(Constants.INTERNET_TYPE_WIFI)) {
                            SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) TonskeProbeActivity.this);
                            saradnikDataService.downloadDataFromLink(Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA, stage.getId(), LocalStorage.getToken(context), TonskeProbeActivity.this);
                        } else {
                            materialRefreshLayout.finishRefresh();
                        }
                    } else {
                        SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) TonskeProbeActivity.this);
                        saradnikDataService.downloadDataFromLink(Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA, stage.getId(), LocalStorage.getToken(context), TonskeProbeActivity.this);
                    }
                } else {
                    setDataOnView(stage.getId());
                }
            }
        });
    }

    @Override
    public void getSaradnikData(String data) {
        try {
            JSONObject result = new JSONObject(data);

            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                JSONObject responseData = new JSONObject(result.getString(Constants.DATA_JSON));
                int stageId = responseData.getInt(Constants.SARADNIK_STAGE_ID_JSON);
                LocalStorage.setLocalData(context, Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA + stageId, result.getString(Constants.DATA_JSON));
                LocalStorage.setSaradnikTonskeProbeSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView(stageId);
            } else {
                materialRefreshLayout.finishRefresh();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setDataOnView (int id) {
        materialRefreshLayout.finishRefresh();
        String localData = LocalStorage.getLocalData(context, Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA + id);
        try {
            JSONObject localDataObject = new JSONObject(localData);
            String tonskeProbe = localDataObject.getString(Constants.DATA_JSON);

            JSONArray jsonArrayTonskeProbe = new JSONArray(tonskeProbe);

            ArrayList<String> listaDatuma = new ArrayList<String>();
            HashMap<String, ArrayList<ArrayList<String>>> listaTonskihProba = new HashMap<String, ArrayList<ArrayList<String>>>();

            for (int i = 0 ; i < jsonArrayTonskeProbe.length() ; i++) {
                JSONObject currentDate = new JSONObject(jsonArrayTonskeProbe.get(i).toString());
                listaDatuma.add(currentDate.getString("date"));
            }

            for(int i = 0; i < listaDatuma.size(); i++) {
                JSONObject currentDate = new JSONObject(jsonArrayTonskeProbe.get(i).toString());
                JSONArray scheduleArray = new JSONArray(currentDate.getString("schedule"));

                ArrayList<ArrayList<String>> subItem = new ArrayList<ArrayList<String>>();
                for(int j = 0; j < scheduleArray.length(); j++) {
                    JSONObject currentSchedule = new JSONObject(scheduleArray.get(j).toString());

                    ArrayList<String> subItemStrings = new ArrayList<String>();
                    subItemStrings.add(currentSchedule.getString("time"));
                    subItemStrings.add(currentSchedule.getString("musician"));
                    subItemStrings.add("Tonska proba: ");

                    subItem.add(subItemStrings);
                }
                listaTonskihProba.put(listaDatuma.get(i), subItem);
            }
            adapter = new ExpListNoSwipeAdapter(context, listaDatuma, listaTonskihProba);
            listView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
