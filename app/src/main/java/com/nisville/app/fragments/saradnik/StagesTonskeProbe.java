package com.nisville.app.fragments.saradnik;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.SaradnikDataService;
import com.nisville.app.other.adapters.TonskeProbeStagesAdapter;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.models.Stage;

public class StagesTonskeProbe extends Fragment implements SaradnikDataService.downloadComplete {


    public StagesTonskeProbe() {
        // Required empty public constructor
    }

    private RecyclerView rv;
    private Context context;
    private MaterialRefreshLayout materialRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_saradnik_tonske_probe, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        getActivity().setTitle("Tonske Probe");

        context = this.getContext();
        materialRefreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.tonskeRefresh);
        int refreshColors[] = {getResources().getColor(R.color.colorPrimary)};
        materialRefreshLayout.setProgressColors(refreshColors);
        rv = (RecyclerView) view.findViewById(R.id.saradnik_tonske_probe_recyclerview);

        setDataOnView();

        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                // CHECK INTERNET CONNECTION AND FIRE REQUEST
                if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                        if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                            SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) StagesTonskeProbe.this);
                            saradnikDataService.downloadDataFromLink(Constants.SARADNIK_STAGES_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                        } else {
                            materialRefreshLayout.finishRefresh();
                        }
                    } else {
                        SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) StagesTonskeProbe.this);
                        saradnikDataService.downloadDataFromLink(Constants.SARADNIK_STAGES_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                    }
                } else {
                    setDataOnView();
                }
            }
        });
        return view;
    }

    @Override
    public void getSaradnikData(String data) {
        try {
            JSONObject result = new JSONObject(data);
            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.SARADNIK_STAGES_LOCAL_STORAGE_DATA, result.getString(Constants.DATA_JSON));
                LocalStorage.setStagesSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView();
            } else {
                materialRefreshLayout.finishRefresh();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setDataOnView () {
        materialRefreshLayout.finishRefresh();
        String localStages = LocalStorage.getLocalData(context, Constants.SARADNIK_STAGES_LOCAL_STORAGE_DATA);
        try {
            JSONArray jsonArrayStages = new JSONArray(localStages);
            ArrayList<Stage> stages = new ArrayList<>();

            for (int i = 0 ; i < jsonArrayStages.length() ; i++) {
                JSONObject currentStage = new JSONObject(jsonArrayStages.get(i).toString());
                Stage newStage = new Stage();

                newStage.setIme(currentStage.getString("ime"));
                newStage.setId(currentStage.getInt("id"));
                stages.add(newStage);
            }

            rv.setHasFixedSize(true);
            TonskeProbeStagesAdapter adapter = new TonskeProbeStagesAdapter(this.getContext(), stages);
            rv.setAdapter(adapter);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            rv.setLayoutManager(llm);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
