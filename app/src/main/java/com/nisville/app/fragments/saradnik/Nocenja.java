package com.nisville.app.fragments.saradnik;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.SaradnikDataService;
import com.nisville.app.other.adapters.ExpListNocenjaAdapter;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.models.Nocenje;


public class Nocenja extends Fragment implements SaradnikDataService.downloadComplete {

    public Nocenja() {
        // Required empty public constructor
    }

    private ExpListNocenjaAdapter adapter;
    private ExpandableListView listView;
    private Context context;
    private MaterialRefreshLayout materialRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_saradnik_nocenja, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        getActivity().setTitle("Noćenja");

        context = this.getContext();
        materialRefreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.nocenjaRefresh);
        int refreshColors[] = {getResources().getColor(R.color.colorPrimary)};
        materialRefreshLayout.setProgressColors(refreshColors);
        listView = (ExpandableListView) view.findViewById(R.id.expListViewNocenje);

        setDataOnView();

        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
            if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                    SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                    saradnikDataService.downloadDataFromLink(Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                }
            } else {
                SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                saradnikDataService.downloadDataFromLink(Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
            }
        } else {
            setDataOnView();
        }

        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                // CHECK INTERNET CONNECTION AND FIRE REQUEST
                if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                        if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                            SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) Nocenja.this);
                            saradnikDataService.downloadDataFromLink(Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                        } else {
                            materialRefreshLayout.finishRefresh();
                        }
                    } else {
                        SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) Nocenja.this);
                        saradnikDataService.downloadDataFromLink(Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                    }
                } else {
                    setDataOnView();
                }
            }
        });
        return view;
    }

    @Override
    public void getSaradnikData(String data) {
        try {
            JSONObject result = new JSONObject(data);

            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA, result.getString(Constants.DATA_JSON));
                LocalStorage.setSaradnikNocenjaSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView();
            } else {
                materialRefreshLayout.finishRefresh();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            materialRefreshLayout.finishRefresh();
        }
    }

    public void setDataOnView () {
        materialRefreshLayout.finishRefresh();
        String nocenja = LocalStorage.getLocalData(context, Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA);
        try {
            JSONArray jsonArrayNocenja = new JSONArray(nocenja);
            ArrayList<String> listaDatuma = new ArrayList<String>();
            HashMap<String, ArrayList<ArrayList<Nocenje>>> listaNocenja = new HashMap<String, ArrayList<ArrayList<Nocenje>>>();

            for (int i = 0 ; i < jsonArrayNocenja.length() ; i++) {
                JSONObject currentDate = new JSONObject(jsonArrayNocenja.get(i).toString());
                listaDatuma.add(currentDate.getString("date"));
            }

            for(int i = 0; i < listaDatuma.size(); i++) {
                JSONObject currentDate = new JSONObject(jsonArrayNocenja.get(i).toString());
                JSONArray nightsArray = new JSONArray(currentDate.getString("nights"));

                ArrayList<ArrayList<Nocenje>> subItem = new ArrayList<ArrayList<Nocenje>>();
                ArrayList<Nocenje> subItemStrings = new ArrayList<Nocenje>();

                for(int j = 0; j < nightsArray.length(); j++) {
                    JSONObject currentNight = new JSONObject(nightsArray.get(j).toString());

                    Nocenje nocenje = new Nocenje();
                    nocenje.setMusician(currentNight.getString("musician"));
                    nocenje.setBrojClanova(currentNight.getInt("broj_clanova"));
                    nocenje.setPocetak(currentNight.getString("pocetak"));
                    nocenje.setKraj(currentNight.getString("kraj"));
                    nocenje.setAccomodation(currentNight.getString("accomodation"));
                    nocenje.setSoba(currentNight.getString("soba"));

                    subItemStrings.add(nocenje);
                }
                subItem.add(subItemStrings);
                listaNocenja.put(listaDatuma.get(i), subItem);
            }
            adapter = new ExpListNocenjaAdapter(context, listaDatuma, listaNocenja);
            listView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
