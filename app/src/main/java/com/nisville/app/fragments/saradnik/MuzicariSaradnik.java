package com.nisville.app.fragments.saradnik;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.SaradnikDataService;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.models.Muzicar;
import com.nisville.app.other.adapters.SaradnikMuzicariAdapter;

public class MuzicariSaradnik extends Fragment implements SaradnikDataService.downloadComplete {

    public MuzicariSaradnik() {
        // Required empty public constructor
    }

    public CardView searchCard;
    private RecyclerView rv;
    private Context context;
    public ArrayList<Muzicar> muzicari;
    public android.widget.SearchView search;
    private MaterialRefreshLayout materialRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_saradnik_muzicari, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        setHasOptionsMenu(true);

        searchCard = (CardView) view.findViewById(R.id.search_card);
        search = (android.widget.SearchView) view.findViewById(R.id.searchMuzicara);
        materialRefreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.muzicariRefresh);
        int refreshColors[] = {getResources().getColor(R.color.colorPrimary)};
        materialRefreshLayout.setProgressColors(refreshColors);

        searchCard.setVisibility(View.GONE);

        SharedPreferences activityPref = this.getActivity().getSharedPreferences("ActivityInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = activityPref.edit();
        edit.putString("activity", "saradnikMuzicari");
        edit.apply();

        context = this.getContext();
        rv = (RecyclerView) view.findViewById(R.id.saradnik_muzicari_recyclerview);

        getActivity().setTitle("Muzičari");

        setDataOnView();

        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
            if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                    SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                    saradnikDataService.downloadDataFromLink(Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                }
            } else {
                SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                saradnikDataService.downloadDataFromLink(Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
            }
        } else {
            setDataOnView();
        }

        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                // CHECK INTERNET CONNECTION AND FIRE REQUEST
                if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                        if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                            SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) MuzicariSaradnik.this);
                            saradnikDataService.downloadDataFromLink(Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                        } else {
                            materialRefreshLayout.finishRefresh();
                        }
                    } else {
                        SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) MuzicariSaradnik.this);
                        saradnikDataService.downloadDataFromLink(Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                    }
                } else {
                    setDataOnView();
                }
            }

        });

        search.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                query = query.toLowerCase();
                ArrayList<Muzicar> filteredList = new ArrayList<Muzicar>();
                for(int i = 0; i < muzicari.size(); i++) {
                    String muzicarIme = muzicari.get(i).getIme().toLowerCase();
                    if(muzicarIme.contains(query)) {
                        filteredList.add(muzicari.get(i));
                    }
                }

                rv.setHasFixedSize(true);
                SaradnikMuzicariAdapter adapter = new SaradnikMuzicariAdapter(getContext(), filteredList);
                rv.setAdapter(adapter);

                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                rv.setLayoutManager(llm);

                return true;
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openSearch() {
        if(searchCard.getVisibility() == View.GONE) {
            searchCard.setVisibility(View.VISIBLE);
            search.setIconifiedByDefault(true);
            search.setIconified(false);
        } else {
            searchCard.setVisibility(View.GONE);
            search.setIconifiedByDefault(false);
            search.setQuery("", true);
        }
    }

    @Override
    public void getSaradnikData(String data) {
        try {
            JSONObject result = new JSONObject(data);
            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA, result.getString(Constants.DATA_JSON));
                LocalStorage.setSaradnikMuzicariSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setDataOnView () {
        materialRefreshLayout.finishRefresh();
        String saradnikMuzicari = LocalStorage.getLocalData(context, Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA);
        try {
            JSONArray jsonArrayMuzicari = new JSONArray(saradnikMuzicari);
            muzicari = new ArrayList<>();

            for (int i = 0 ; i < jsonArrayMuzicari.length() ; i++) {
                JSONObject currentMuzicar = new JSONObject(jsonArrayMuzicari.get(i).toString());
                Muzicar noviMuzicar = new Muzicar();
                noviMuzicar.setId(currentMuzicar.getInt("id"));
                noviMuzicar.setIme(currentMuzicar.getString("ime"));
                noviMuzicar.setDrzava(currentMuzicar.getString("poreklo"));
                String datum = "";
                if(currentMuzicar.getString("dolazak_datum").equals(Constants.NULL_RESPONSE)) {
                    datum = Constants.NIJE_UNETO_LABEL;
                } else {
                    datum += currentMuzicar.getString("dolazak_datum");
                }
                if(!currentMuzicar.getString("odlazak_datum").equals(Constants.NULL_RESPONSE)) {
                    datum += " - " + currentMuzicar.getString("odlazak_datum");
                }
                noviMuzicar.setDatum(datum);
                muzicari.add(noviMuzicar);
            }

            rv.setHasFixedSize(true);
            SaradnikMuzicariAdapter adapter = new SaradnikMuzicariAdapter(getContext(), muzicari);
            rv.setAdapter(adapter);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            rv.setLayoutManager(llm);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
