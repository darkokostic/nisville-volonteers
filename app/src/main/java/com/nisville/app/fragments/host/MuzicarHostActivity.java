package com.nisville.app.fragments.host;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.nisville.app.R;
import com.nisville.app.other.adapters.muzicarsingleview.HostoviListAdapter;
import com.nisville.app.other.adapters.muzicarsingleview.LetoviSliderAdapter;
import com.nisville.app.other.adapters.muzicarsingleview.NastupiSliderAdapter;
import com.nisville.app.other.adapters.muzicarsingleview.PrevoziSliderAdapter;
import com.nisville.app.other.adapters.muzicarsingleview.SmestajSliderAdapter;
import com.nisville.app.other.helpers.CallNumber;
import com.nisville.app.other.helpers.SMSSend;
import com.nisville.app.other.helpers.ShareContent;
import com.nisville.app.other.httpservices.HostDataService;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.models.Muzicar;
import com.nisville.app.other.models.Volonter;

public class MuzicarHostActivity extends AppCompatActivity implements HostDataService.downloadComplete {

    public TextView txtIme;
    public TextView txtKontaktOsoba;
    public TextView txtTelefon;
    public TextView txtBrojClanova;
    public TextView txtEmail;
    public TextView txtPress;
    public TextView txtRadionica;
    public TextView txtDolazak;
    public TextView txtOdlazak;
    public TextView txtNapomena;
    private RecyclerView hostoviRv;
    private HostoviListAdapter hostoviAdapter;
    private NastupiSliderAdapter nastupiAdapter;
    private ExpandableListView nastupiExpListView;
    private SmestajSliderAdapter smestajAdapter;
    private ExpandableListView smestajExpListView;
    private PrevoziSliderAdapter prevoziAdapter;
    private ExpandableListView prevoziExpListView;
    private LetoviSliderAdapter letoviAdapter;
    private ExpandableListView letoviExpListView;
    private ImageView btnCallContactPerson;
    private ImageView btnMessageContactPerson;
    private ArrayList<Volonter> hosts;
    private ArrayList<String> nastupiShareList;
    private ArrayList<String> smestajShareList;
    private ArrayList<String> prevoziShareList;
    private ArrayList<String> letoviShareList;
    public int id;
    public Context context;
    public LinearLayout header;
    public LinearLayout content;
    public LinearLayout retryLayout;
    public Button retry;
    private boolean isCheckmarksVisible;
    private LinearLayout layoutShareButton;
    private Button btnShareChosen;
    // Share CheckBoxes
    private CheckBox cbShareIme;
    private CheckBox cbShareKontaktOsoba;
    private CheckBox cbShareTelefon;
    private CheckBox cbShareBrojClanova;
    private CheckBox cbShareEmail;
    private CheckBox cbSharePress;
    private CheckBox cbShareRadionica;
    private CheckBox cbShareDolazak;
    private CheckBox cbShareOdlazak;
    private CheckBox cbShareNapomena;
    private LinearLayout layoutBrojTelefona;
    private LinearLayout layoutKontaktOsoba;
    private CardView cardViewBrojClanova;
    private CardView cardViewEmail;
    private CardView cardViewPress;
    private CardView cardViewRadionica;
    private CardView cardViewDolazak;
    private CardView cardViewOdlazak;
    private CardView cardViewNapomena;
    private ArrayList<Volonter> selectedHosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host_muzicar);
        StatusBarUtil.setColor(MuzicarHostActivity.this, getResources().getColor(R.color.colorPrimary));

        Toolbar toolbar = (Toolbar) findViewById(R.id.host_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        header = (LinearLayout) findViewById(R.id.host_muzicar_header);
        content = (LinearLayout) findViewById(R.id.host_muzicar_content);
        retryLayout = (LinearLayout) findViewById(R.id.host_muzicar_retry);
        retry = (Button) findViewById(R.id.btnHostMuzicarSingleRetry);
        txtIme = (TextView) findViewById(R.id.txtHostMuzicarSingleIme);
        txtKontaktOsoba = (TextView) findViewById(R.id.txtHostMuzicarSingleKontaktOsoba);
        txtTelefon = (TextView) findViewById(R.id.txtHostMuzicarSingleTelefon);
        txtBrojClanova = (TextView) findViewById(R.id.txtHostMuzicarSingleBrojClanova);
        txtEmail = (TextView) findViewById(R.id.txtHostMuzicarSingleEmail);
        txtPress = (TextView) findViewById(R.id.txtHostMuzicarSinglePress);
        txtRadionica = (TextView) findViewById(R.id.txtHostMuzicarSingleRadionica);
        hostoviRv = (RecyclerView) findViewById(R.id.host_muzicar_hostovi_recycle_view);
        nastupiExpListView = (ExpandableListView) findViewById(R.id.expListViewMuzicarNastupi);
        smestajExpListView = (ExpandableListView) findViewById(R.id.expListViewMuzicarSmestaj);
        txtDolazak = (TextView) findViewById(R.id.txtHostMuzicarSingleDolazak);
        txtOdlazak = (TextView) findViewById(R.id.txtHostMuzicarSingleOdlazak);
        prevoziExpListView = (ExpandableListView) findViewById(R.id.expListViewMuzicarPrevozi);
        letoviExpListView = (ExpandableListView) findViewById(R.id.expListViewMuzicarLetovi);
        txtNapomena = (TextView) findViewById(R.id.txtHostMuzicarSingleNapomena);
        btnCallContactPerson = (ImageView) findViewById(R.id.btnCallContactPerson);
        btnMessageContactPerson = (ImageView) findViewById(R.id.btnMessageContactPerson);
        cbShareIme = (CheckBox) findViewById(R.id.cbShareIme);
        cbShareKontaktOsoba = (CheckBox) findViewById(R.id.cbShareKontaktOsoba);
        cbShareTelefon = (CheckBox) findViewById(R.id.cbShareTelefon);
        cbShareBrojClanova = (CheckBox) findViewById(R.id.cbShareBrojClanova);
        cbShareEmail = (CheckBox) findViewById(R.id.cbShareEmail);
        cbSharePress = (CheckBox) findViewById(R.id.cbSharePress);
        cbShareRadionica = (CheckBox) findViewById(R.id.cbShareRadionica);
        cbShareDolazak = (CheckBox) findViewById(R.id.cbShareDolazak);
        cbShareOdlazak = (CheckBox) findViewById(R.id.cbShareOdlazak);
        cbShareNapomena = (CheckBox) findViewById(R.id.cbShareNapomena);
        layoutShareButton = (LinearLayout) findViewById(R.id.layoutShareButton);
        btnShareChosen = (Button) findViewById(R.id.btnshareChosen);
        layoutBrojTelefona = (LinearLayout) findViewById(R.id.broj_telefona_layout);
        layoutKontaktOsoba = (LinearLayout) findViewById(R.id.kontakt_osoba_layout);
        cardViewBrojClanova = (CardView) findViewById(R.id.card_view_broj_clanova);
        cardViewEmail = (CardView) findViewById(R.id.card_view_email);
        cardViewPress = (CardView) findViewById(R.id.card_view_press);
        cardViewRadionica = (CardView) findViewById(R.id.card_radionica);
        cardViewDolazak = (CardView) findViewById(R.id.card_view_dolazak);
        cardViewOdlazak = (CardView) findViewById(R.id.card_view_odlazak);
        cardViewNapomena = (CardView) findViewById(R.id.card_view_napomena);

        hostoviAdapter = new HostoviListAdapter(context, hosts, new HostoviListAdapter.OnItemCheckListener() {
            @Override
            public void onItemCheck(Volonter item) {}

            @Override
            public void onItemUncheck(Volonter item) {}
        });

        nastupiAdapter = new NastupiSliderAdapter(context, null, null);
        letoviAdapter = new LetoviSliderAdapter(context, null, null);
        prevoziAdapter = new PrevoziSliderAdapter(context, null, null);
        smestajAdapter = new SmestajSliderAdapter(context, null, null);

        ArrayList<Muzicar> data = (ArrayList<Muzicar>) getIntent().getSerializableExtra("listaMuzicara");
        Muzicar muzicar = data.get(getIntent().getIntExtra("idMuzicara", 0));

        id = muzicar.getId();
        setTitle(muzicar.getIme());

        setCheckmarksVisibillityGone();

        header.setVisibility(View.GONE);
        content.setVisibility(View.GONE);
        retryLayout.setVisibility(View.GONE);
        setDataOnView(id);

        cbShareIme.setChecked(true);
        cbShareIme.setClickable(false);

        layoutBrojTelefona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbShareTelefon.setChecked(!cbShareTelefon.isChecked());
            }
        });

        layoutKontaktOsoba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbShareKontaktOsoba.setChecked(!cbShareKontaktOsoba.isChecked());
            }
        });

        cardViewBrojClanova.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbShareBrojClanova.setChecked(!cbShareBrojClanova.isChecked());
            }
        });

        cardViewEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbShareEmail.setChecked(!cbShareEmail.isChecked());
            }
        });

        cardViewPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbSharePress.setChecked(!cbSharePress.isChecked());
            }
        });

        cardViewRadionica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbShareRadionica.setChecked(!cbShareRadionica.isChecked());
            }
        });

        cardViewDolazak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbShareDolazak.setChecked(!cbShareDolazak.isChecked());
            }
        });

        cardViewOdlazak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbShareOdlazak.setChecked(!cbShareOdlazak.isChecked());
            }
        });

        cardViewNapomena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbShareNapomena.setChecked(!cbShareNapomena.isChecked());
            }
        });

        btnShareChosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareChosen();
            }
        });
        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if (CheckNetworkConnection.isNetworkAvailable(this)) {
            if (LocalStorage.getToggleButtonState(this).equals("enabled")) {
                if (CheckNetworkConnection.checkConnectionType(this).equals(Constants.INTERNET_TYPE_WIFI)) {
                    HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) this);
                    hostDataService.downloadDataFromLink(Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA, id, LocalStorage.getToken(context), this);
                }
            } else {
                HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) this);
                hostDataService.downloadDataFromLink(Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA, id, LocalStorage.getToken(context), this);
            }
        } else {
            if (LocalStorage.checkIfMuzicarExist(this, id)) {
                setDataOnView(id);
            } else {
                retryLayout.setVisibility(View.VISIBLE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // CHECK INTERNET CONNECTION AND FIRE REQUEST
                        if (CheckNetworkConnection.isNetworkAvailable(context)) {
                            if (LocalStorage.getToggleButtonState(context).equals("enabled")) {
                                if (CheckNetworkConnection.checkConnectionType(context).equals(Constants.INTERNET_TYPE_WIFI)) {
                                    HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) context);
                                    hostDataService.downloadDataFromLink(Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA, id, LocalStorage.getToken(context), MuzicarHostActivity.this);
                                }
                            } else {
                                HostDataService hostDataService = new HostDataService((HostDataService.downloadComplete) context);
                                hostDataService.downloadDataFromLink(Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA, id, LocalStorage.getToken(context), MuzicarHostActivity.this);
                            }
                        }
                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.host_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.host_action_share_all:
                setCheckmarksVisibillityGone();
                shareAll();
                return true;
            case R.id.host_action_share_chosen:
                if(isCheckmarksVisible) {
                    setCheckmarksVisibillityGone();
                } else {
                    setCheckmarksVisibillityVisible();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void shareAll() {
        String hostsList = "";
        for(int i = 0; i < hosts.size(); i++) {
            String brojTelefona = "";
            if(hosts.get(i).getBrojTelefona() != "null") {
                brojTelefona = ", " + hosts.get(i).getBrojTelefona();
            }
            hostsList += "Host " + (i + 1) + ": " + hosts.get(i).getIme() + brojTelefona + "\n";
        }
        String nastupiList = "";
        for(int i = 0; i < nastupiShareList.size(); i++) {
            nastupiList += "Nastup " + (i + 1) + ": " + "\n" + nastupiShareList.get(i) + "\n";
        }

        String smestajList = "";
        for(int i = 0; i < smestajShareList.size(); i++) {
            smestajList += "Smeštaj " + (i + 1) + ": " + "\n" + smestajShareList.get(i) + "\n";
        }

        String prevozList = "";
        for(int i = 0; i < prevoziShareList.size(); i++) {
            prevozList += "Prevoz " + (i + 1) + ": " + "\n" + prevoziShareList.get(i) + "\n";
        }

        String letoviList = "";
        for(int i = 0; i < letoviShareList.size(); i++) {
            letoviList += "Let " + (i + 1) + ": " + "\n" + letoviShareList.get(i) + "\n";
        }

        ShareContent.share(context, "Muzičar: " + txtIme.getText().toString() + "\n" +
                                    "Kontakt osoba: " + txtKontaktOsoba.getText().toString() + "\n" +
                                    "Telefon: " + txtTelefon.getText().toString() + "\n" +
                                    "Broj članova: " + txtBrojClanova.getText().toString() + "\n" +
                                    "E-mail: " + txtEmail.getText().toString() + "\n" +
                                    "Dolazak: " + txtDolazak.getText().toString() + "\n" +
                                    "Odlazak: " + txtOdlazak.getText().toString() + "\n" +
                                    hostsList + "\n" +
                                    nastupiList + "\n" +
                                    smestajList + "\n" +
                                    prevozList + "\n" +
                                    letoviList + "\n" +
                                    "Napomena: " + txtNapomena.getText().toString() + "\n");
    }

    @Override
    public void getData(String data) {
        try {
            JSONObject result = new JSONObject(data);
            if (!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA + id, result.getString(Constants.DATA_JSON));
                LocalStorage.setMuzicarSingleHostSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));
                setDataOnView(id);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setDataOnView (int id) {
        retryLayout.setVisibility(View.GONE);
        String localData = LocalStorage.getLocalData(context, Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA + id);
        try {
            JSONObject muzicar = new JSONObject(localData);

            txtIme.setText(muzicar.getString("ime"));
            this.setTitle(muzicar.getString("ime"));
            txtKontaktOsoba.setText(muzicar.getString("kontakt_osoba"));
            txtTelefon.setText(muzicar.getString("broj_telefona"));
            if(!muzicar.getString("broj_clanova").equals(Constants.NULL_RESPONSE)) {
                txtBrojClanova.setText(muzicar.getString("broj_clanova"));
            } else {
                txtBrojClanova.setText(Constants.NIJE_UNETO_LABEL);
            }
            txtEmail.setText(muzicar.getString("mail"));

            btnCallContactPerson.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!txtTelefon.getText().equals(Constants.NIJE_UNETO_LABEL)) {
                        CallNumber.call(context, txtTelefon.getText().toString());
                    } else {
                        Toast.makeText(MuzicarHostActivity.this, Constants.NO_PHONE_NUMBER_MESSAGE, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btnMessageContactPerson.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!txtTelefon.getText().equals(Constants.NIJE_UNETO_LABEL)) {
                        SMSSend.send(context, txtTelefon.getText().toString());
                    } else {
                        Toast.makeText(MuzicarHostActivity.this, Constants.NO_PHONE_NUMBER_MESSAGE, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            // Hostovi lista
            JSONArray jsonArrayHosts = new JSONArray(muzicar.getString("hosts"));
            hosts = new ArrayList<>();

            for (int i = 0 ; i < jsonArrayHosts.length() ; i++) {
                JSONObject currentHost = new JSONObject(jsonArrayHosts.get(i).toString());
                Volonter newHost = new Volonter();

                newHost.setIme(currentHost.getString("name"));
                newHost.setBrojTelefona(currentHost.getString("broj_telefona"));

                hosts.add(newHost);
            }

            selectedHosts = new ArrayList<>();
            hostoviRv.setHasFixedSize(true);
            hostoviAdapter = new HostoviListAdapter(context, hosts, new HostoviListAdapter.OnItemCheckListener() {
                @Override
                public void onItemCheck(Volonter item) {
                    selectedHosts.add(item);
                }

                @Override
                public void onItemUncheck(Volonter item) {
                    selectedHosts.remove(item);
                }
            });
            hostoviRv.setAdapter(hostoviAdapter);
            LinearLayoutManager llm = new LinearLayoutManager(context);
            hostoviRv.setLayoutManager(llm);

            // Press i radionice
            if(!muzicar.getString("press").equals(Constants.NULL_RESPONSE)) {
                JSONObject press = new JSONObject(muzicar.getString("press"));
                txtPress.setText(press.getString("date") + " u " + press.getString("time"));
            } else {
                txtPress.setText(Constants.NIJE_UNETO_LABEL);
            }
            if(!muzicar.getString("workshop").equals(Constants.NULL_RESPONSE)) {
                JSONObject radionica = new JSONObject(muzicar.getString("workshop"));
                txtRadionica.setText(radionica.getString("date") + " u " + radionica.getString("time"));
            } else {
                txtRadionica.setText(Constants.NIJE_UNETO_LABEL);
            }

            // Nastup Slider
            List<String> nastupiHeader = new ArrayList<String>();
            HashMap<String, List<String>> nastupiSliderData = new HashMap<String, List<String>>();

            nastupiHeader.add("Nastupi");
            List<String> sliderData = new ArrayList<String>();
            sliderData.add(muzicar.getString("performances"));
            nastupiSliderData.put(nastupiHeader.get(0), sliderData);
            nastupiAdapter = new NastupiSliderAdapter(context, nastupiHeader, nastupiSliderData);

            nastupiExpListView.setAdapter(nastupiAdapter);
            nastupiExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {
                    setListViewHeight(parent, groupPosition);
                    return false;
                }
            });

            // Smestaj Slider
            List<String> smestajHeader = new ArrayList<String>();
            HashMap<String, List<String>> smestajSliderData = new HashMap<String, List<String>>();

            if(!muzicar.getString("smestaj").equals(Constants.NULL_RESPONSE)) {
                smestajHeader.add("Smeštaj: " + muzicar.getString("smestaj"));
            } else {
                smestajHeader.add("Smeštaj");
            }
            List<String> smestajList = new ArrayList<String>();
            smestajList.add(muzicar.getString("nights"));
            smestajSliderData.put(smestajHeader.get(0), smestajList);
            smestajAdapter = new SmestajSliderAdapter(context, smestajHeader, smestajSliderData);

            smestajExpListView.setAdapter(smestajAdapter);
            smestajExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {
                    setListViewHeight(parent, groupPosition);
                    return false;
                }
            });

            // Dolazak
            String dolazak = "";
            String dolazakDatum = "";
            if(!muzicar.getString("dolazak").equals(Constants.NULL_RESPONSE)) {
                dolazak = muzicar.getString("dolazak");
            } else {
                dolazak = Constants.NIJE_UNETO_LABEL;
            }
            if(!muzicar.getString("dolazak_datum").equals(Constants.NULL_RESPONSE)) {
                dolazak += "\n" + muzicar.getString("dolazak_datum");
            }
            txtDolazak.setText(dolazak);

            // Odlazak
            String odlazak = "";
            String odlazakDatum = "";
            if(!muzicar.getString("odlazak").equals(Constants.NULL_RESPONSE)) {
                odlazak = muzicar.getString("odlazak");
            } else {
                odlazak = Constants.NIJE_UNETO_LABEL;
            }
            if(!muzicar.getString("odlazak_datum").equals(Constants.NULL_RESPONSE)) {
                odlazak += "\n" + muzicar.getString("odlazak_datum");
            }
            txtOdlazak.setText(odlazak);

            // Prevozi Slider
            List<String> prevoziHeader = new ArrayList<String>();
            HashMap<String, List<String>> prevoziSliderData = new HashMap<String, List<String>>();

            prevoziHeader.add("Prevozi");
            List<String> prevoziList = new ArrayList<String>();
            prevoziList.add(muzicar.getString("transports"));
            prevoziSliderData.put(prevoziHeader.get(0), prevoziList);
            prevoziAdapter = new PrevoziSliderAdapter(context, prevoziHeader, prevoziSliderData);

            prevoziExpListView.setAdapter(prevoziAdapter);
            prevoziExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {
                    setListViewHeight(parent, groupPosition);
                    return false;
                }
            });

            // Letovi Slider
            List<String> letoviHeader = new ArrayList<String>();
            HashMap<String, List<String>> letoviSliderData = new HashMap<String, List<String>>();

            letoviHeader.add("Letovi");
            List<String> letoviList = new ArrayList<String>();
            letoviList.add(muzicar.getString("flights"));
            letoviSliderData.put(letoviHeader.get(0), letoviList);
            letoviAdapter = new LetoviSliderAdapter(context, letoviHeader, letoviSliderData);

            letoviExpListView.setAdapter(letoviAdapter);
            letoviExpListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {
                    setListViewHeight(parent, groupPosition);
                    return false;
                }
            });

            // Napomena
            txtNapomena.setText(muzicar.getString("javna_napomena"));

            // Lists for sharing
            // Nastupi
            JSONArray jsonArrayNastupi = new JSONArray(muzicar.getString("performances"));
            nastupiShareList = new ArrayList<>();

            for (int i = 0; i < jsonArrayNastupi.length(); i++) {
                JSONObject currentNastup = new JSONObject(jsonArrayNastupi.get(i).toString());
                JSONObject stage = new JSONObject(currentNastup.getString("stage"));
                String nastup = "Stage: " + stage.getString("ime") + "\n";
                if(!currentNastup.getString("trajanje").equals(Constants.NULL_RESPONSE)) {
                    nastup += "Trajanje: " + currentNastup.getString("trajanje") + "\n";
                }
                if(!currentNastup.getString("vreme").equals(Constants.NIJE_UNETO_LABEL)) {
                    JSONObject vreme = new JSONObject(currentNastup.getString("vreme"));
                    nastup += "Početak: " + vreme.getString("date") + " u " + vreme.getString("time") + "\n";
                }
                if(!currentNastup.getString("zavrsetak").equals(Constants.NULL_RESPONSE)) {
                    JSONObject zavrsetak = new JSONObject(currentNastup.getString("zavrsetak"));
                    nastup += "Završetak: " + zavrsetak.getString("date") + " u " + zavrsetak.getString("time") + "\n";
                }
                if(!currentNastup.getString("soundcheck").equals(Constants.NIJE_UNETO_LABEL)) {
                    JSONObject tonska = new JSONObject(currentNastup.getString("soundcheck"));
                    nastup += "Tonska proba: " + tonska.getString("date") + " u " + tonska.getString("time");
                }
                nastupiShareList.add(nastup);
            }

            // Smestaj
            JSONArray jsonArraySmestaj = new JSONArray(muzicar.getString("nights"));
            smestajShareList = new ArrayList<>();

            for (int i = 0; i < jsonArraySmestaj.length(); i++) {
                JSONObject currentSmestaj = new JSONObject(jsonArraySmestaj.get(i).toString());
                String smestaj = "";
                if(!currentSmestaj.getString("accomodation").equals(Constants.NULL_RESPONSE)) {
                    JSONObject hotel = new JSONObject(currentSmestaj.getString("accomodation"));
                    smestaj += "Hotel: " + hotel.getString("ime") + "\n";
                }

                if(!currentSmestaj.getString("brojljudi").equals(Constants.NULL_RESPONSE)) {
                    smestaj += "Broj ljudi: " + currentSmestaj.getInt("brojljudi") + "" + "\n";
                }

                if(!currentSmestaj.getString("soba").equals(Constants.NULL_RESPONSE)) {
                    smestaj += "Soba: " + currentSmestaj.getString("soba") + "\n";
                }

                if(!currentSmestaj.getString("pocetak").equals(Constants.NULL_RESPONSE)) {
                    smestaj += "Početak: " + currentSmestaj.getString("pocetak") + "\n";
                }

                if(!currentSmestaj.getString("kraj").equals(Constants.NULL_RESPONSE)) {
                    smestaj += "Kraj: " + currentSmestaj.getString("kraj");
                }

                smestajShareList.add(smestaj);
            }

            // Prevozi
            JSONArray jsonArrayPrevozi = new JSONArray(muzicar.getString("transports"));
            prevoziShareList = new ArrayList<>();

            for (int i = 0; i < jsonArrayPrevozi.length(); i++) {
                String prevoz = "";
                JSONObject currentPrevoz = new JSONObject(jsonArrayPrevozi.get(i).toString());
                if(!currentPrevoz.getString("muzicar").equals(Constants.NULL_RESPONSE)) {
                    JSONObject prevozMuzicar = new JSONObject(currentPrevoz.getString("muzicar"));
                    if(!prevozMuzicar.getString("ime").equals(Constants.NULL_RESPONSE)) {
                        prevoz += "Muzičar: " + prevozMuzicar.getString("ime") + "\n";
                    }
                }
                if(!currentPrevoz.getString("ucesnik").equals(Constants.NULL_RESPONSE)) {
                    JSONObject ucesnik = new JSONObject(currentPrevoz.getString("ucesnik"));
                    if(!ucesnik.getString("name").equals(Constants.NULL_RESPONSE)) {
                        prevoz += "Učesnik: " + ucesnik.getString("name") + "\n";
                    }
                }

                if (!currentPrevoz.getString("polazak").equals(Constants.NULL_RESPONSE)) {
                    JSONObject polazakVreme = new JSONObject(currentPrevoz.getString("polazak"));
                    prevoz += "Polazak: " + currentPrevoz.getString("polazak_mesto") + ", " + polazakVreme.getString("date") + " u " + polazakVreme.getString("time") + "\n";
                }

                if(!currentPrevoz.getString("dolazak").equals(Constants.NULL_RESPONSE)) {
                    JSONObject dolazakVreme = new JSONObject(currentPrevoz.getString("dolazak"));
                    prevoz += "Dolazak: " + currentPrevoz.getString("dolazak_mesto") + ", " + dolazakVreme.getString("date") + " u " + dolazakVreme.getString("time") + "\n";
                }

                if (!currentPrevoz.getString("br_ljudi").equals(Constants.NULL_RESPONSE)) {
                    prevoz += "Broj ljudi: " + currentPrevoz.getString("br_ljudi") + "" + "\n";
                }

                if(!currentPrevoz.getString("vozac").equals(Constants.NULL_RESPONSE) && !currentPrevoz.getString("vozac_br").equals(Constants.NULL_RESPONSE)) {
                    prevoz += "Vozač: " + currentPrevoz.getString("vozac") + ", " + currentPrevoz.getString("vozac_br") + "\n";
                }

                if(!currentPrevoz.getString("vehicle").equals(Constants.NULL_RESPONSE)) {
                    JSONObject vozilo = new JSONObject(currentPrevoz.getString("vehicle"));
                    prevoz += "Vozilo: " + vozilo.getString("tip");
                }

                prevoziShareList.add(prevoz);
            }

            // Letovi
            JSONArray jsonArrayLetovi = new JSONArray(muzicar.getString("flights"));
            letoviShareList = new ArrayList<>();

            for (int i = 0; i < jsonArrayLetovi.length(); i++) {
                JSONObject currentLet = new JSONObject(jsonArrayLetovi.get(i).toString());
                String let = "";
                let += "Broj leta: " + currentLet.getString("br_let") + "\n";
                if(!currentLet.getString("poletanje").equals(Constants.NULL_RESPONSE)) {
                    JSONObject poletanje = new JSONObject(currentLet.getString("poletanje"));
                    let += "Poletanje: " + poletanje.getString("date") + ", " + poletanje.getString("time") + "\n";
                }
                let += "Iz: " + currentLet.getString("od") + "\n";
                if(!currentLet.getString("sletanje").equals(Constants.NULL_RESPONSE)) {
                    JSONObject sletanje = new JSONObject(currentLet.getString("sletanje"));
                    let += "Sletanje: " + sletanje.getString("date") + ", " + sletanje.getString("time") + "\n";
                }
                let += "U: " + currentLet.getString("do") + "\n";
                let += "Broj ljudi: " + currentLet.getString("br_men");
                letoviShareList.add(let);
            }

            header.setVisibility(View.VISIBLE);
            content.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void setCheckmarksVisibillityGone() {
        this.isCheckmarksVisible = false;
        this.cbShareKontaktOsoba.setChecked(false);
        this.cbShareTelefon.setChecked(false);
        this.cbShareEmail.setChecked(false);
        this.cbShareBrojClanova.setChecked(false);
        this.cbSharePress.setChecked(false);
        this.cbShareRadionica.setChecked(false);
        this.cbShareDolazak.setChecked(false);
        this.cbShareOdlazak.setChecked(false);
        this.cbShareNapomena.setChecked(false);
        this.layoutBrojTelefona.setClickable(false);
        this.layoutKontaktOsoba.setClickable(false);
        this.cardViewBrojClanova.setClickable(false);
        this.cardViewEmail.setClickable(false);
        this.cardViewPress.setClickable(false);
        this.cardViewRadionica.setClickable(false);
        this.cardViewDolazak.setClickable(false);
        this.cardViewOdlazak.setClickable(false);
        this.cardViewNapomena.setClickable(false);
        this.hostoviAdapter.setCheckBoxVisibility(false);
        this.nastupiAdapter.setCheckBoxVisibility(false);
        this.prevoziAdapter.setCheckBoxVisibility(false);
        this.letoviAdapter.setCheckBoxVisibility(false);
        this.smestajAdapter.setCheckBoxVisibility(false);
        this.btnMessageContactPerson.setVisibility(View.VISIBLE);
        this.btnCallContactPerson.setVisibility(View.VISIBLE);
        this.cbShareIme.setVisibility(View.GONE);
        this.cbShareKontaktOsoba.setVisibility(View.GONE);
        this.cbShareTelefon.setVisibility(View.GONE);
        this.cbShareEmail.setVisibility(View.GONE);
        this.cbShareBrojClanova.setVisibility(View.GONE);
        this.cbSharePress.setVisibility(View.GONE);
        this.cbShareRadionica.setVisibility(View.GONE);
        this.cbShareDolazak.setVisibility(View.GONE);
        this.cbShareOdlazak.setVisibility(View.GONE);
        this.cbShareNapomena.setVisibility(View.GONE);
        this.layoutShareButton.setVisibility(View.GONE);
    }

    public void setCheckmarksVisibillityVisible() {
        this.isCheckmarksVisible = true;
        this.cbShareKontaktOsoba.setChecked(false);
        this.cbShareTelefon.setChecked(false);
        this.cbShareEmail.setChecked(false);
        this.cbShareBrojClanova.setChecked(false);
        this.cbSharePress.setChecked(false);
        this.cbShareRadionica.setChecked(false);
        this.cbShareDolazak.setChecked(false);
        this.cbShareOdlazak.setChecked(false);
        this.cbShareNapomena.setChecked(false);
        this.layoutBrojTelefona.setClickable(true);
        this.layoutKontaktOsoba.setClickable(true);
        this.cardViewBrojClanova.setClickable(true);
        this.cardViewEmail.setClickable(true);
        this.cardViewPress.setClickable(true);
        this.cardViewRadionica.setClickable(true);
        this.cardViewDolazak.setClickable(true);
        this.cardViewOdlazak.setClickable(true);
        this.cardViewNapomena.setClickable(true);
        this.hostoviAdapter.setCheckBoxVisibility(true);
        this.nastupiAdapter.setCheckBoxVisibility(true);
        this.prevoziAdapter.setCheckBoxVisibility(true);
        this.letoviAdapter.setCheckBoxVisibility(true);
        this.smestajAdapter.setCheckBoxVisibility(true);
        this.btnMessageContactPerson.setVisibility(View.GONE);
        this.btnCallContactPerson.setVisibility(View.GONE);
        this.cbShareIme.setVisibility(View.VISIBLE);
        this.cbShareKontaktOsoba.setVisibility(View.VISIBLE);
        this.cbShareTelefon.setVisibility(View.VISIBLE);
        this.cbShareEmail.setVisibility(View.VISIBLE);
        this.cbShareBrojClanova.setVisibility(View.VISIBLE);
        this.cbSharePress.setVisibility(View.VISIBLE);
        this.cbShareRadionica.setVisibility(View.VISIBLE);
        this.cbShareDolazak.setVisibility(View.VISIBLE);
        this.cbShareOdlazak.setVisibility(View.VISIBLE);
        this.cbShareNapomena.setVisibility(View.VISIBLE);
        this.layoutShareButton.setVisibility(View.VISIBLE);
    }

    public void shareChosen() {
        String chosenData = "";
        if(cbShareIme.isChecked()) {
            chosenData += "Muzičar: " + txtIme.getText().toString() + "\n";
        }
        if(cbShareKontaktOsoba.isChecked()) {
            chosenData += "Kontakt osoba: " + txtKontaktOsoba.getText().toString() + "\n";
        }
        if(cbShareTelefon.isChecked()) {
            chosenData += "Telefon: " + txtTelefon.getText().toString() + "\n";
        }
        if(cbShareBrojClanova.isChecked()) {
            chosenData += "Broj članova: " + txtBrojClanova.getText().toString() + "\n";
        }
        if(cbShareEmail.isChecked()) {
            chosenData += "E-mail: " + txtEmail.getText().toString() + "\n";
        }
        if(cbShareDolazak.isChecked()) {
            chosenData += "Dolazak: " + txtDolazak.getText().toString() + "\n";
        }
        if(cbShareOdlazak.isChecked()) {
            chosenData += "Odlazak: " + txtOdlazak.getText().toString() + "\n";
        }
        if(cbShareNapomena.isChecked()) {
            chosenData += "Napomena: " + txtNapomena.getText().toString() + "\n";
        }
        if(selectedHosts.size() > 0) {
            for(int i = 0; i < selectedHosts.size(); i++) {
                chosenData += "Host " + (i + 1) + ": " + selectedHosts.get(i).getIme() + ", " + selectedHosts.get(i).getBrojTelefona() + "\n";
            }
        }
        if(nastupiAdapter.getIsShareable()) {
            for(int i = 0; i < nastupiShareList.size(); i++) {
                chosenData += "Nastup " + (i + 1) + ": " + "\n" + nastupiShareList.get(i) + "\n";
            }
        }
        if(smestajAdapter.getIsShareable()) {
            for(int i = 0; i < smestajShareList.size(); i++) {
                chosenData += "Smeštaj " + (i + 1) + ": " + "\n" + smestajShareList.get(i) + "\n";
            }
        }
        if(prevoziAdapter.getIsShareable()) {
            for(int i = 0; i < prevoziShareList.size(); i++) {
                chosenData += "Prevoz " + (i + 1) + ": " + "\n" + prevoziShareList.get(i) + "\n";
            }
        }
        if(letoviAdapter.getIsShareable()) {
            for(int i = 0; i < letoviShareList.size(); i++) {
                chosenData += "Let " + (i + 1) + ": " + "\n" + letoviShareList.get(i) + "\n";
            }
        }
        ShareContent.share(context, chosenData);
        setCheckmarksVisibillityGone();
    }
}
