package com.nisville.app.fragments.saradnik;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.SaradnikDataService;
import com.nisville.app.other.adapters.ExpListNoSwipeAdapter;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;

public class PressKonferencije extends Fragment implements SaradnikDataService.downloadComplete {

    public PressKonferencije() {
        // Required empty public constructor
    }

    private ExpListNoSwipeAdapter adapter;
    private ExpandableListView listView;
    private Context context;
    private MaterialRefreshLayout materialRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_saradnik_press_konferencija, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        getActivity().setTitle("Press Konferencija");

        context = this.getContext();
        materialRefreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.pressRefresh);
        int refreshColors[] = {getResources().getColor(R.color.colorPrimary)};
        materialRefreshLayout.setProgressColors(refreshColors);
        listView = (ExpandableListView) view.findViewById(R.id.expListViewPressKonferencija);

        setDataOnView();

        // CHECK INTERNET CONNECTION AND FIRE REQUEST
        if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
            if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                    SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                    saradnikDataService.downloadDataFromLink(Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                }
            } else {
                SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) this);
                saradnikDataService.downloadDataFromLink(Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
            }
        } else {
            setDataOnView();
        }

        materialRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                // CHECK INTERNET CONNECTION AND FIRE REQUEST
                if(CheckNetworkConnection.isNetworkAvailable(getActivity())) {
                    if(LocalStorage.getToggleButtonState(getActivity()).equals("enabled")) {
                        if(CheckNetworkConnection.checkConnectionType(getActivity()).equals(Constants.INTERNET_TYPE_WIFI)) {
                            SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) PressKonferencije.this);
                            saradnikDataService.downloadDataFromLink(Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                        } else {
                            materialRefreshLayout.finishRefresh();
                        }
                    } else {
                        SaradnikDataService saradnikDataService = new SaradnikDataService((SaradnikDataService.downloadComplete) PressKonferencije.this);
                        saradnikDataService.downloadDataFromLink(Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA, 0, LocalStorage.getToken(context), getActivity());
                    }
                } else {
                    setDataOnView();
                }
            }
        });
        return view;
    }

    @Override
    public void getSaradnikData(String data) {
        try {
            JSONObject result = new JSONObject(data);
            if(!result.getString(Constants.DATA_JSON).equals(Constants.UP_TO_DATE_JSON)) {
                LocalStorage.setLocalData(context, Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA, result.getString(Constants.DATA_JSON));
                LocalStorage.setSaradnikPressSyncTime(context, result.getString(Constants.LAST_SYNC_JSON));

                setDataOnView();
            } else {
                materialRefreshLayout.finishRefresh();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setDataOnView () {
        materialRefreshLayout.finishRefresh();
        String konferencije = LocalStorage.getLocalData(context, Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA);

        try {
            JSONArray jsonArrayKonferencije = new JSONArray(konferencije);
            ArrayList<String> listaDatuma = new ArrayList<String>();
            HashMap<String, ArrayList<ArrayList<String>>> listaKonferencija = new HashMap<String, ArrayList<ArrayList<String>>>();

            for (int i = 0 ; i < jsonArrayKonferencije.length() ; i++) {
                JSONObject currentDate = new JSONObject(jsonArrayKonferencije.get(i).toString());
                listaDatuma.add(currentDate.getString("date"));
            }

            for(int i = 0; i < listaDatuma.size(); i++) {
                JSONObject currentDate = new JSONObject(jsonArrayKonferencije.get(i).toString());
                JSONArray scheduleArray = new JSONArray(currentDate.getString("schedule"));

                ArrayList<ArrayList<String>> subItem = new ArrayList<ArrayList<String>>();

                for(int j = 0; j < scheduleArray.length(); j++) {
                    JSONObject currentSchedule = new JSONObject(scheduleArray.get(j).toString());

                    ArrayList<String> subItemStrings = new ArrayList<String>();
                    subItemStrings.add(currentSchedule.getString("time"));
                    subItemStrings.add(currentSchedule.getString("musician"));
                    subItemStrings.add("Press konferencija: ");

                    subItem.add(subItemStrings);
                }
                listaKonferencija.put(listaDatuma.get(i), subItem);
            }
            adapter = new ExpListNoSwipeAdapter(this.getContext(), listaDatuma, listaKonferencija);
            listView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
