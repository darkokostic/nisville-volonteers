package com.nisville.app.activitiy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import com.nisville.app.R;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;
import com.nisville.app.other.httpservices.ChangePasswordService;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordService.changePassword {

    private EditText etNewPassword, etRepeatedPassword;
    private Button btnChange;
    private String token;
    ChangePasswordService changePasswordService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        changePasswordService = new ChangePasswordService((ChangePasswordService.changePassword) this);

        btnChange = (Button) findViewById(R.id.btnChange);
        etNewPassword = (EditText) findViewById(R.id.etNewPassword);
        etRepeatedPassword = (EditText) findViewById(R.id.etRepeatNewPassword);

        token = getIntent().getStringExtra(Constants.TOKEN);

        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPassword = etNewPassword.getText().toString();
                String repeatedPassword = etRepeatedPassword.getText().toString();

                if(newPassword.equals("") || newPassword.equals(null)) {
                    Toast.makeText(getApplicationContext(), Constants.ENTER_PASSWORD, Toast.LENGTH_SHORT).show();
                } else if(repeatedPassword.equals("") || repeatedPassword.equals(null)) {
                    Toast.makeText(getApplicationContext(), Constants.REPEATE_PASSWORD, Toast.LENGTH_SHORT).show();
                } else if(newPassword.equals(repeatedPassword)) {
                    if(newPassword.length() < 8) {
                        Toast.makeText(getApplicationContext(), Constants.LENGTH_PASSWORD_MESSAGE, Toast.LENGTH_SHORT).show();
                    } else if(newPassword.equals(newPassword.toLowerCase())) {
                        Toast.makeText(getApplicationContext(), Constants.UPPER_LETTER_MESSAGE, Toast.LENGTH_SHORT).show();
                    } else if(newPassword.equals(newPassword.toUpperCase())) {
                        Toast.makeText(getApplicationContext(), Constants.LOWER_LETTER_MESSAGE, Toast.LENGTH_SHORT).show();
                    } else if(!Pattern.compile("[0-9]").matcher(newPassword).find()) {
                        Toast.makeText(getApplicationContext(), Constants.NUMBER_LETTER_MESSAGE, Toast.LENGTH_SHORT).show();
                    } else {
                        if(CheckNetworkConnection.isNetworkAvailable(getApplicationContext())) {
                            try {
                                JSONObject jsonParam = new JSONObject();
                                jsonParam.put("new_pass", newPassword);
                                changePasswordService.changePassword(jsonParam, token, ChangePasswordActivity.this);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), Constants.NETWORK_CONNECTION_ALERT_MESSAGE, Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Constants.PASSWORD_MATCH, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void getResponse(String data) {
        try {
            JSONObject response = new JSONObject(data);
            if (response.getString(Constants.OUTCOME).equals(Constants.OUTCOME_FAILED)) {
                Toast.makeText(getApplicationContext(), Constants.CHANGE_PASSWORD_TOKEN_EXPIRED, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), Constants.PASSWORD_SUCCESSFULLY_CHANGED, Toast.LENGTH_SHORT).show();
                JSONObject result = new JSONObject(response.getString(Constants.RESULT));
                JSONObject displayData = new JSONObject(result.getString(Constants.DISPLAY_DATA));
                LocalStorage.setLocalData(ChangePasswordActivity.this, Constants.USER_NAME, displayData.getString(Constants.NAME_JSON));
                LocalStorage.setLocalData(ChangePasswordActivity.this, Constants.USER_TYPE, displayData.getString(Constants.TYPE));
                LocalStorage.setLocalData(ChangePasswordActivity.this, Constants.USER_ACCESS, result.getString(Constants.ACCESS));
                LocalStorage.setToken(ChangePasswordActivity.this, result.getString(Constants.TOKEN_JSON));

                if (result.getString(Constants.ACCESS).equals(Constants.USER_TYPE_HOST)) {
                    hostFullSync(result.getString(Constants.SYNC));
                } else {
                    saradnikFullSync(result.getString(Constants.SYNC));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void hostFullSync(String data) {
        try {
            JSONObject fullData = new JSONObject(data);
            LocalStorage.setLocalData(this, Constants.HOST_FULL_SYNC_LOCAL_STORAGE_DATA, fullData.toString());
            LocalStorage.setLocalData(this, Constants.HOST_POCETNA_LOCAL_STORAGE_DATA, fullData.getString(Constants.HOST_HOME_JSON));
            LocalStorage.setLocalData(this, Constants.HOST_MUZICARI_LOCAL_STORAGE_DATA, fullData.getString(Constants.HOST_MOJI_MUZICARI_JSON));
            LocalStorage.setLocalData(this, Constants.HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA, fullData.getString(Constants.HOST_BROJEVI_TELEFONA_JSON));

            JSONArray muzicari = new JSONArray(fullData.getString(Constants.HOST_MOJI_MUZICARI_JSON));

            for(int i = 0; i < muzicari.length(); i++) {
                JSONObject currentMuzicar = new JSONObject(muzicari.get(i).toString());
                int id = currentMuzicar.getInt("id");
                LocalStorage.setLocalData(this, Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA + id, muzicari.get(i).toString());
            }

            LocalStorage.setHostFullSyncTime(this, fullData.getString(Constants.LAST_SYNC_JSON));

            Toast.makeText(getApplicationContext(), Constants.SUCCESSFULLY_LOGGED_IN, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), HostHomeActivity.class));
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void saradnikFullSync(String data) {
        try {
            JSONObject fullData = new JSONObject(data);

            LocalStorage.setLocalData(this, Constants.SARADNIK_FULL_SYNC_LOCAL_STORAGE_DATA, fullData.toString());
            LocalStorage.setLocalData(this, Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_MUZICARI_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_PRESS_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_WORKSHOP_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_WORKSHOP_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_PREVOZ_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_NOCENJA_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_STAGES_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_STAGES_JSON));

            JSONArray muzicari = new JSONArray(fullData.getString(Constants.SARADNIK_MUZICARI_JSON));

            for(int i = 0; i < muzicari.length(); i++) {
                JSONObject currentMuzicar = new JSONObject(muzicari.get(i).toString());
                int id = currentMuzicar.getInt("id");
                LocalStorage.setLocalData(this, Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA + id, muzicari.get(i).toString());
            }

            JSONArray stages = new JSONArray(fullData.getString(Constants.SARADNIK_STAGES_JSON));

            for(int i = 0; i < stages.length(); i++) {
                JSONObject currentStage = new JSONObject(stages.get(i).toString());
                int id = currentStage.getInt("id");
                LocalStorage.setLocalData(this, Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA + id, currentStage.getString("tonske"));
                LocalStorage.setLocalData(this, Constants.SARADNIK_NASTUPI_LOCAL_STORAGE_DATA + id, currentStage.getString("nastupi"));
            }

            LocalStorage.setSaradnikFullSyncTime(this, fullData.getString(Constants.LAST_SYNC_JSON));

            Toast.makeText(getApplicationContext(), Constants.SUCCESSFULLY_LOGGED_IN, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), SaradnikHomeActivity.class));
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
