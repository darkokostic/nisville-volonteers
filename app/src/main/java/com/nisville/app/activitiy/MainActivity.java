package com.nisville.app.activitiy;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

import com.jaeger.library.StatusBarUtil;

import com.nisville.app.R;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;

public class MainActivity extends AppCompatActivity {
    private Context context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.context = this;
        StatusBarUtil.setColor(MainActivity.this, getResources().getColor(R.color.colorPrimary));

        CountDownTimer timer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {}
            @Override
            public void onFinish() {
                if(LocalStorage.checkIfTokenExist(context)) {
                    if(LocalStorage.getLocalData(context, Constants.USER_ACCESS).equals(Constants.USER_TYPE_HOST)) {
                        startActivity(new Intent(MainActivity.this, HostHomeActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(MainActivity.this, SaradnikHomeActivity.class));
                        finish();
                    }
                } else {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }.start();
    }
}
