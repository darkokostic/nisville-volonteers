package com.nisville.app.activitiy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import com.nisville.app.R;
import com.nisville.app.fragments.saradnik.HomeSaradnik;
import com.nisville.app.fragments.saradnik.MuzicariSaradnik;
import com.nisville.app.fragments.saradnik.StagesNastupi;
import com.nisville.app.fragments.saradnik.Nocenja;
import com.nisville.app.fragments.saradnik.PressKonferencije;
import com.nisville.app.fragments.saradnik.Prevozi;
import com.nisville.app.fragments.saradnik.Radionice;
import com.nisville.app.fragments.saradnik.StagesTonskeProbe;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;

public class SaradnikHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public Switch onlyWifiButton;
    public TextView txtUsername;
    public TextView txtType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saradnik_home);
        StatusBarUtil.setColor(SaradnikHomeActivity.this, getResources().getColor(R.color.colorPrimary));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));

        onlyWifiButton = (Switch) navigationView.getMenu().getItem(12).getActionView().findViewById(R.id.toggleButton);
        View headerView =  navigationView.getHeaderView(0);
        txtUsername = (TextView) headerView.findViewById(R.id.txt_saradnik_username);
        txtType = (TextView) headerView.findViewById(R.id.txt_saradnik_type);
        txtUsername.setText(LocalStorage.getLocalData(this, Constants.USER_NAME));
        txtType.setText(LocalStorage.getLocalData(this, Constants.USER_TYPE));

        // CHECK ONLY WIFI BUTTON STATE
        if(LocalStorage.checkIfToggleStateExist(this)) {
            if(LocalStorage.getToggleButtonState(this).equals("enabled")) {
                onlyWifiButton.setChecked(true);
            } else {
                onlyWifiButton.setChecked(false);
            }
        } else {
            LocalStorage.setToggleButtonState(getApplicationContext(), "disabled");
            onlyWifiButton.setChecked(false);
        }

        // ONLY WIFI BUTTON CLICK LISTENER
        onlyWifiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onlyWifiButton.isChecked()) {
                    LocalStorage.setToggleButtonState(getApplicationContext(), "enabled");
                } else {
                    LocalStorage.setToggleButtonState(getApplicationContext(), "disabled");
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        MenuItem menuItem = navigationView.getMenu().findItem(R.id.saradnik_pocetna);

        if(!menuItem.isChecked()) {
            menuItem.setChecked(true);
            onNavigationItemSelected(navigationView.getMenu().getItem(0));
        } else {
            super.onBackPressed();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        switch (id) {
            case R.id.saradnik_pocetna:
                HomeSaradnik homeSaradnikFragment = new HomeSaradnik();
                fragmentTransaction.replace(R.id.saradnik_layout, homeSaradnikFragment);
                fragmentTransaction.commit();
                break;
            case R.id.saradnik_muzicari:
                LocalStorage.setActiveFragment(this, Constants.SARADNIK_FRAGMENT_MUZICARI);
                MuzicariSaradnik muzicariSaradnik = new MuzicariSaradnik();
                fragmentTransaction.replace(R.id.saradnik_layout, muzicariSaradnik);
                fragmentTransaction.commit();
                break;
            case R.id.saradnik_tonske_probe:
                LocalStorage.setActiveFragment(this, Constants.SARADNIK_FRAGMENT_TONSKE_PROBE);
                StagesTonskeProbe stagesTonskeProbe = new StagesTonskeProbe();
                fragmentTransaction.replace(R.id.saradnik_layout, stagesTonskeProbe);
                fragmentTransaction.commit();
                break;
            case R.id.saradnik_nastupi:
                LocalStorage.setActiveFragment(this, Constants.SARADNIK_FRAGMENT_NASTUPI);
                StagesNastupi stagesNastupi = new StagesNastupi();
                fragmentTransaction.replace(R.id.saradnik_layout, stagesNastupi);
                fragmentTransaction.commit();
                break;
            case R.id.saradnik_press_konferencija:
                LocalStorage.setActiveFragment(this, Constants.SARADNIK_FRAGMENT_PRESS);
                PressKonferencije pressKonferencije = new PressKonferencije();
                fragmentTransaction.replace(R.id.saradnik_layout, pressKonferencije);
                fragmentTransaction.commit();
                break;
            case R.id.saradnik_prevozi:
                LocalStorage.setActiveFragment(this, Constants.SARADNIK_FRAGMENT_PREVOZI);
                Prevozi prevozi = new Prevozi();
                fragmentTransaction.replace(R.id.saradnik_layout, prevozi);
                fragmentTransaction.commit();
                break;
            case R.id.saradnik_nocenja:
                LocalStorage.setActiveFragment(this, Constants.SARADNIK_FRAGMENT_NOCENJA);
                Nocenja nocenja = new Nocenja();
                fragmentTransaction.replace(R.id.saradnik_layout, nocenja);
                fragmentTransaction.commit();
                break;
            case R.id.saradnik_radionice:
                LocalStorage.setActiveFragment(this, Constants.SARADNIK_FRAGMENT_RADIONICE);
                Radionice radionice = new Radionice();
                fragmentTransaction.replace(R.id.saradnik_layout, radionice);
                fragmentTransaction.commit();
                break;
            case R.id.saradnik_odjavi_se:
                LocalStorage.removeToken(this);
                Toast.makeText(getApplicationContext(), Constants.SUCCESSFULLY_LOGGED_OUT, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, LoginActivity.class));
                this.finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        switch(LocalStorage.getActiveFragment(this)) {
            case Constants.SARADNIK_FRAGMENT_MUZICARI:
                navigationView.getMenu().getItem(1).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(1));
                break;
            case Constants.SARADNIK_FRAGMENT_TONSKE_PROBE:
                navigationView.getMenu().getItem(2).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(2));
                break;
            case Constants.SARADNIK_FRAGMENT_NASTUPI:
                navigationView.getMenu().getItem(3).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(3));
                break;
            case Constants.SARADNIK_FRAGMENT_PRESS:
                navigationView.getMenu().getItem(4).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(4));
                break;
            case Constants.SARADNIK_FRAGMENT_RADIONICE:
                navigationView.getMenu().getItem(5).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(5));
                break;
            case Constants.SARADNIK_FRAGMENT_PREVOZI:
                navigationView.getMenu().getItem(6).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(6));
                break;
            default:
                navigationView.getMenu().getItem(0).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(0));
        }
    }
}
