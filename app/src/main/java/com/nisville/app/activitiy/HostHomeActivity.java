package com.nisville.app.activitiy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import com.nisville.app.R;
import com.nisville.app.fragments.host.BrojeviTelefonaActivity;
import com.nisville.app.fragments.host.HomeHost;
import com.nisville.app.fragments.host.MuzicariHost;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;

public class HostHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    public Switch onlyWifiButton;
    public TextView txtUsername;
    public TextView txtType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_host_home);
        StatusBarUtil.setColor(HostHomeActivity.this, getResources().getColor(R.color.colorPrimary));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));

        onlyWifiButton = (Switch) navigationView.getMenu().getItem(7).getActionView().findViewById(R.id.toggleButton);
        View headerView =  navigationView.getHeaderView(0);
        txtUsername = (TextView) headerView.findViewById(R.id.txt_host_username);
        txtType = (TextView) headerView.findViewById(R.id.txt_host_type);
        txtUsername.setText(LocalStorage.getLocalData(this, Constants.USER_NAME));
        txtType.setText(LocalStorage.getLocalData(this, Constants.USER_TYPE));

        // CHECK ONLY WIFI BUTTON STATE
        if(LocalStorage.checkIfToggleStateExist(this)) {
            if(LocalStorage.getToggleButtonState(this).equals("enabled")) {
                onlyWifiButton.setChecked(true);
            } else {
                onlyWifiButton.setChecked(false);
            }
        } else {
            LocalStorage.setToggleButtonState(getApplicationContext(), "disabled");
            onlyWifiButton.setChecked(false);
        }

        // ONLY WIFI BUTTON CLICK LISTENER
        onlyWifiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onlyWifiButton.isChecked()) {
                    LocalStorage.setToggleButtonState(getApplicationContext(), "enabled");
                } else {
                    LocalStorage.setToggleButtonState(getApplicationContext(), "disabled");
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        MenuItem menuItem = navigationView.getMenu().findItem(R.id.host_pocetna);

        if(!menuItem.isChecked()) {
            menuItem.setChecked(true);
            onNavigationItemSelected(navigationView.getMenu().getItem(0));
        } else {
            super.onBackPressed();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        switch (LocalStorage.getActiveFragment(this)) {
            case Constants.HOST_FRAGMENT_MUZICARI:
                navigationView.getMenu().getItem(1).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(1));
                break;
            default:
                navigationView.getMenu().getItem(0).setChecked(true);
                onNavigationItemSelected(navigationView.getMenu().getItem(0));
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        switch (id) {
            case R.id.host_pocetna:
                HomeHost homeHost = new HomeHost();
                fragmentTransaction.replace(R.id.host_layout, homeHost);
                fragmentTransaction.commit();
                break;
            case R.id.host_moji_muzicari:
                LocalStorage.setActiveFragment(this, Constants.HOST_FRAGMENT_MUZICARI);
                MuzicariHost muzicariHost = new MuzicariHost();
                fragmentTransaction.replace(R.id.host_layout, muzicariHost);
                fragmentTransaction.commit();
                break;
            case R.id.host_brojevi_telefona:
                startActivity(new Intent(getApplicationContext(), BrojeviTelefonaActivity.class));
                break;
            case R.id.host_odjavi_se:
                LocalStorage.removeToken(this);
                Toast.makeText(getApplicationContext(), Constants.SUCCESSFULLY_LOGGED_OUT, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, LoginActivity.class));
                this.finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
