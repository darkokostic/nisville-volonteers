package com.nisville.app.activitiy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nisville.app.R;
import com.nisville.app.other.httpservices.LoginService;
import com.nisville.app.other.helpers.CheckNetworkConnection;
import com.nisville.app.other.helpers.Constants;
import com.nisville.app.other.helpers.LocalStorage;

public class LoginActivity extends AppCompatActivity implements LoginService.login {

    private EditText etUsername, etPassword;
    private Button btnLogin;
    private LoginService loginService;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        StatusBarUtil.setColor(LoginActivity.this, getResources().getColor(R.color.colorPrimary));

        SharedPreferences activityPref = getSharedPreferences("ActivityInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = activityPref.edit();
        edit.putString("activity", "home");
        edit.apply();

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        builder = new AlertDialog.Builder(this);
        loginService = new LoginService((LoginService.login) this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                if(username.equals("") || username.equals(null)) {
                    Toast.makeText(getApplicationContext(), Constants.NO_USERNAME_MESSAGE, Toast.LENGTH_SHORT).show();
                } else if(password.equals("") || password.equals(null)) {
                    Toast.makeText(getApplicationContext(), Constants.NO_PASSWORD_MESSAGE, Toast.LENGTH_SHORT).show();
                } else {
                    if(CheckNetworkConnection.isNetworkAvailable(getApplicationContext())) {
                        try {
                            JSONObject jsonParam = new JSONObject();
                            jsonParam.put("email", username);
                            jsonParam.put("password", password);

                            loginService.login(jsonParam, LoginActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), Constants.NETWORK_CONNECTION_ALERT_MESSAGE, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void getResponse(String data) {
        try {
            JSONObject response = new JSONObject(data);
            if(response.getString(Constants.OUTCOME).equals(Constants.OUTCOME_SUCCESS)) {
                JSONObject result = new JSONObject(response.getString(Constants.RESULT));
                JSONObject displayData = new JSONObject(result.getString(Constants.DISPLAY_DATA));
                LocalStorage.setLocalData(LoginActivity.this, Constants.USER_NAME, displayData.getString(Constants.NAME_JSON));
                LocalStorage.setLocalData(LoginActivity.this, Constants.USER_TYPE, displayData.getString(Constants.TYPE));
                LocalStorage.setLocalData(LoginActivity.this, Constants.USER_ACCESS, result.getString(Constants.ACCESS));
                LocalStorage.setToken(LoginActivity.this, result.getString(Constants.TOKEN_JSON));

                if(result.getString(Constants.ACCESS).equals(Constants.USER_TYPE_HOST)) {
                    hostFullSync(result.getString(Constants.SYNC));
                } else {
                    saradnikFullSync(result.getString(Constants.SYNC));
                }
            } else {
                Intent i = new Intent(LoginActivity.this, ChangePasswordActivity.class);
                i.putExtra(Constants.TOKEN, response.getString(Constants.PASSWORD_CHANGE_TOKEN));
                startActivity(i);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void hostFullSync(String data) {
        try {
            JSONObject fullData = new JSONObject(data);

            LocalStorage.setLocalData(this, Constants.HOST_FULL_SYNC_LOCAL_STORAGE_DATA, fullData.toString());
            LocalStorage.setLocalData(this, Constants.HOST_POCETNA_LOCAL_STORAGE_DATA, fullData.getString(Constants.HOST_HOME_JSON));
            LocalStorage.setLocalData(this, Constants.HOST_MUZICARI_LOCAL_STORAGE_DATA, fullData.getString(Constants.HOST_MOJI_MUZICARI_JSON));
            LocalStorage.setLocalData(this, Constants.HOST_BROJEVI_TELEFONA_LOCAL_STORAGE_DATA, fullData.getString(Constants.HOST_BROJEVI_TELEFONA_JSON));

            JSONArray muzicari = new JSONArray(fullData.getString(Constants.HOST_MOJI_MUZICARI_JSON));

            for(int i = 0; i < muzicari.length(); i++) {
                JSONObject currentMuzicar = new JSONObject(muzicari.get(i).toString());
                int id = currentMuzicar.getInt("id");
                LocalStorage.setLocalData(this, Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA + id, muzicari.get(i).toString());
            }

            LocalStorage.setHostFullSyncTime(this, fullData.getString(Constants.LAST_SYNC_JSON));

            Toast.makeText(getApplicationContext(), Constants.SUCCESSFULLY_LOGGED_IN, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), HostHomeActivity.class));
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void saradnikFullSync(String data) {
        try {
            JSONObject fullData = new JSONObject(data);

            LocalStorage.setLocalData(this, Constants.SARADNIK_FULL_SYNC_LOCAL_STORAGE_DATA, fullData.toString());
            LocalStorage.setLocalData(this, Constants.SARADNIK_MUZICARI_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_MUZICARI_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_PRESS_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_PRESS_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_WORKSHOP_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_WORKSHOP_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_PREVOZ_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_PREVOZ_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_NOCENJA_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_NOCENJA_JSON));
            LocalStorage.setLocalData(this, Constants.SARADNIK_STAGES_LOCAL_STORAGE_DATA, fullData.getString(Constants.SARADNIK_STAGES_JSON));

            JSONArray muzicari = new JSONArray(fullData.getString(Constants.SARADNIK_MUZICARI_JSON));

            for(int i = 0; i < muzicari.length(); i++) {
                JSONObject currentMuzicar = new JSONObject(muzicari.get(i).toString());
                int id = currentMuzicar.getInt("id");
                LocalStorage.setLocalData(this, Constants.MUZICAR_SINGLE_LOCAL_STORAGE_DATA + id, muzicari.get(i).toString());
            }

            JSONArray stages = new JSONArray(fullData.getString(Constants.SARADNIK_STAGES_JSON));

            for(int i = 0; i < stages.length(); i++) {
                JSONObject currentStage = new JSONObject(stages.get(i).toString());
                int id = currentStage.getInt("id");
                LocalStorage.setLocalData(this, Constants.SARADNIK_TONSKE_PROBE_LOCAL_STORAGE_DATA + id, currentStage.getString("tonske"));
                LocalStorage.setLocalData(this, Constants.SARADNIK_NASTUPI_LOCAL_STORAGE_DATA + id, currentStage.getString("nastupi"));
            }

            LocalStorage.setSaradnikFullSyncTime(this, fullData.getString(Constants.LAST_SYNC_JSON));

            Toast.makeText(getApplicationContext(), Constants.SUCCESSFULLY_LOGGED_IN, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), SaradnikHomeActivity.class));
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
